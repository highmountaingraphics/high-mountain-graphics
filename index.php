<!doctype html>
<html class="no-js" lang="en">
  <head>
    <?php include "templates.php"; ?>
    <title>Digital and Offset Printing - Eco-Friendly Printer | High Mountain Graphics</title>
    <?php echo $head; ?>
  </head>
  <body class="collapsing_header">
    <?php echo $header; ?>
    <section id="slider_wrapper" class="slider_wrapper full_page_photo">
      <div id="main_flexslider" class="flexslider">
        <ul class="slides">
          <li class="item" style="background-image: url(images/index/rushmore.jpg)">
            <div class="container">
              <div class="carousel-caption">
                <h1><strong>If Ideas</strong> Can<strong> Move Mountains&hellip;
                What Moves Ideas?</strong></h1>
              </div>
            </div>
          </li>
          <li class="item" style="background-image: url(images/index/greatfalls.jpg)">
            <div class="container">
              <div class="carousel-caption">
                <h1><strong>Consider</strong> the<strong> Nature</strong> <br>of your <strong>Communications</strong></h1>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </section>
    <div class="main">
      <section class="call_to_action">
        <div class="container">
          <h3><strong>Printing</strong> with the <strong>Environment</strong> in <strong>Mind…</strong></h3>
          <h4>We specialize in <strong>quality offset</strong> and <strong>digital printing</strong> for businesses and individuals.</h4>
          <a class="btn btn-primary btn-lg" href="forms/estimate/form.php">Request an Estimate!</a>
        </div>
      </section>
      <section class="features_teasers_wrapper">
        <div class="container">
          <div class="row">
            <div class="feature_teaser col-sm-4 col-md-4">
              <img src="images/index/design_icon.png" alt="design">
              <h3>GRAPHIC DESIGN</h3>
              <p>Planting <strong>personality</strong> into your product or service is just one way we <strong>give your business an edge</strong>. We offer <strong>innovative</strong> ways to advance your brand and vision.</p>
            </div>
            <div class="feature_teaser col-sm-4 col-md-4"> <img src="images/index/printing_icon.png" alt="printing">
              <h3>PRINTING SOLUTIONS</h3>
              <p>When <strong>color</strong> is important, we <strong>specialize</strong> in custom <strong>Pantone<sup>®</sup> Spot Color</strong> printing. When is a priority, digital printing is a cost-effective solution.</p>
            </div>
            <div class="feature_teaser col-sm-4 col-md-4">
              <img src="images/index/web_icon.png" alt="responsive web development">
              <h3>WEB DEVELOPMENT</h3>
              <p><strong>Our team</strong> of experienced <strong>designers</strong> and <strong>developers</strong> can design, host &amp; launch a professional website that accomplishes all of your business objectives.</p>
            </div>
          </div>
        </div>
      </section>
      <section class="portfolio_teasers_wrapper">
        <div class="container">
          <h2 class="section_header fancy centered">When it comes to experience…We have mountains of it!
          <small style="margin: 0px 2%;">Our unquenchable and passionate drive allows us to research continuously, seeking out solutions that meet and exceed our client’s marketing and business objectives.</small>
          </h2>
          <div class="portfolio_strict row">
            <div class="col-sm-4 col-md-4">
              <div class="portfolio_item wow animated flipInX">
                <a href="portfolio/hmg.php" target="_blank" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/index/hmg-bro.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                    <p>At High Mountain Graphics, we thrive on the idea that design makes a difference. Our designs are renewable, our practices responsible, and our products sustainable.</p>
                    <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="portfolio/hmg.php" target="_blank">Tri-fold Brochure</a></h3>
                  <p>Graphic Design</p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-md-4">
              <div class="portfolio_item wow animated flipInX">
                <a href="portfolio/hmg.php" target="_blank" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/index/hmg-bc.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                    <p>When color is important, we specialize in one &amp; two color custom Pantone® Spot Color printing. When turnaround is a priority, digital printing is a cost-effective solution.</p>
                    <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="portfolio/hmg.php" target="_blank">Business Cards</a></h3>
                  <p>Printing Solutions</p>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-md-4">
              <div class="portfolio_item wow animated flipInX">
                <a href="https://www.brokencartons.com" target="_blank" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/products/custom-websites.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                    <p>Our team of experienced designers and developers can build a professional looking website that accomplishes all of your personal and business objectives.</p>
                    <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="https://www.brokencartons.com" target="_blank">www.BrokenCartons.com</a></h3>
                  <p>Web Development</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="clients_section wow animated fadeInUp">
        <div class="container">
          <h2 class="section_header elegant centered">Our Clients &nbsp; &middot; &nbsp; Our Friends &nbsp; &middot; &nbsp; Our Partners</h2>
          <div class="clients_list">
            <a href="https://www.annmariegianni.com/" target="_blank"><img src="images/index/clients/annmarie.jpg" alt="AnnMarie Organic Skin Care"></a>
            <a href="https://www.route46chryslerjeepdodge.com/" target="_blank"><img src="images/index/clients/route-46-cjd.jpg" alt="Route 46 Chrysler Jeep Dodge logo"></a>
            <a href="http://htcnj.org/" target="_blank"><img src="images/index/clients/htcnj.jpg" alt="Healing the children"></a>
            <a href="https://www.brokencartons.com" target="_blank"><img src="images/index/clients/brokencartons.jpg" alt="Brokencartons"></a>
            <a href="https://www.si.edu/" target="_blank"><img src="images/index/clients/smithsonian.jpg" alt="Smithsonian"></a>
            <a href="https://www.suryabrasilproducts.com/" target="_blank"><img src="images/index/clients/surya-brasil.jpg" alt="Surya Brasil logo"></a>
          </div>
        </div>
      </section>
      <footer>
        <section class="twitter_feed_wrapper">
          <div class="container">
            <div class="row wow animated fadeInUp">
              <div class="twitter_feed_icon col-sm-1 col-md-1"><a href="#twitter"><i class="fa fa-twitter"></i></a></div>
              <div class="col-sm-11 col-md-11">
                <blockquote>
                  <p>High Mountain Graphics has exceeded all expectation for our printing needs! We searched for a very long time and went through many printing companies before landing on these amazing people. Rick and Terri are so friendly and have gone above and beyond with every project. As a company, there was still a lot to learn on our end about the 'printing/paper' world and Rick has been so patient and knowledgeable. They are a blessing to our company!! I HIGHLY recommend.</p>
                  — Annmarie Gianni S. <a href="http://www.annmariegianni.com/" target="_blank">AnnmarieGianni.com</a>
                  <br />
                  <br />
                  <a href="./testimonials.php" target="_blank">See All Testimonials</a>
                </blockquote>
              </div>
            </div>
          </div>
        </section>
        <?php echo $copyright; ?>
      </footer>
      </div>
      <?php echo $scripts; ?>
      <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
':35729/livereload.js?snipver=2"></' + 'script>')</script>
    </body>
  </html>
