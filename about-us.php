<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
  <title>About HMG</title>
  <?php echo $head; ?>

  <style media="screen">
    blockquote {
      border-left: 1px solid #999999;
      margin: 0 0 12px;
    }

    blockquote small {
      color: #333;
    }

    .hgroup h1 {
      font-size: 38px;
    }

    .hgroup h2 {
      font-size: 22px;
      margin: 0;
    }

    .col-sm-8.col-md-8 > blockquote {
      border: 0;
    }

    @media screen and (min-width: 990px) {
      .col-sm-8.col-md-8 > blockquote {
        width: 87%;
      }
    }

    .service_teaser.vertical .service_details p {
      font-size: 14px;
    }

    .call_to_action > div:nth-child(1) > h3:nth-child(1) {
      font-size: 46px;
      margin: 0 auto;
      width: 90%;
    }

    .section_header.fancy.centered > small {
      margin: 0 auto;
      width: 98%;
    }
  </style>
</head>

<body class="collapsing_header">
  <?php echo $header; ?>
  <section id="slider_wrapper" class="slider_wrapper full_page_photo">
    <div id="main_flexslider" class="flexslider">
      <ul class="slides">
        <li class="item" style="background-image: url(images/about/ladyliberty.jpg)">
          <div class="container">
            <div class="carousel-caption">
              <h1>We are <strong>proud</strong> of the <strong>strong roots</strong> we've <strong>built</strong> in our <strong>community</strong></h1>
              <!-- <p class="lead skincolored">and are <strong>committed</strong> to extending this service online!</p> -->
            </div>
          </div>
        </li>
        <!-- <li class="item" style="background-image: url(images/about/hmg5thave07506.jpg)">
          <div class="container">
            <div class="carousel-caption">
              <h1><strong>We pride ourselves</strong> on being the <strong>complete source</strong></h1>
              <p class="lead skincolored">for all of your <strong>graphic</strong> and <strong>printing</strong> needs!</p>
            </div>
          </div>
        </li> -->
        <li class="item" style="background-image: url(images/index/turbine.jpg)">
          <div class="container">
            <div class="carousel-caption">
              <h1 style="width: 74%">Our <strong>Commitment</strong><br> to the <strong>Environment</strong></h1>
              <!-- <p class="lead skincolored">Through endless hours of <strong>research</strong> we continue to <strong>educate</strong> ourselves, our staff, and our clients to <strong>improve</strong> on <strong>our goal</strong> of providing the <strong>best</strong> in <strong>graphic communications</strong>.</p> -->
            </div>
          </div>
        </li>
      </ul>
    </div>
  </section>
  <div class="main">
    <section class="hgroup">
      <div class="container">
        <h1>About us</h1>
        <h2><strong>High Mountain Graphics</strong> is a Northern New Jersey Firm specializing in <em>Environmentally Friendly Concept to Completion Graphic Communications</em> for businesses and individuals throughout the Northeast and abroad.</h2>
      </div>
    </section>
    <section class="article-text">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-md-8">
            <p style="width: 93%;">Growing concerns with the environment have prompted us to do our part and act responsibly as a company. By adopting green practices and guidelines we minimize impact on the environment and retain maximum quality and value for the client. By choosing certified recycled paper, soy ink and low voc solvents, we're truly doing more with less. Using certified paper and ink is a testimony of our commitment to provide intelligent choices for our clients while promoting eco-friendly practices for sustainability.</p>
            <p style="width: 93%;">When it comes to experience, we have mountains of it. Our unquenchable and passionate drive allows us to research continuously, seeking out eco-friendly solutions that meet and exceed our client’s marketing and business objectives. We pride ourselves on being the complete source for all of your graphic and printing needs. Choosing to work with High Mountain Graphics means partnering with a reliable company that is environmentally responsible.</p>
            <blockquote>
              <small><strong>OUR MISSION</strong> is to promote environmentally friendly, socially beneficial, and cost-effective Graphic Communication Services.</small>
            </blockquote>
            <blockquote>
              <small><strong>OUR PROMISE</strong> is to provide prompt &amp; courteous service, exceptional quality, and competitive pricing to maximize advertising budgets.</small>
            </blockquote>
            <blockquote>
              <small><strong>OUR GOAL</strong>is to lead with a passion that inspires others to adopt environmental ethics of preservation and conservation.</small>
            </blockquote>
            <blockquote>
              <small><strong>OUR COMMITMENT</strong>to be good stewards of the earth and its resources while striving to preserve and protect the world in which we live.</small>
            </blockquote>
          </div>
          <div class="col-sm-4 col-md-4">
            <div class="service_teaser vertical">
              <div class="service_photo">
                <figure style="background-image:url(images/about/hmg-fam.jpg)"></figure>
              </div>
              <div class="service_details">
                <h2 class="section_header skincolored"><strong>Family Owned <br />&amp; Operated</strong></h2>
                <p>Strategically located at the foothills of High Mountain Preserve—lies High Mountain Graphics, an environmentally friendly graphics, printing and marketing agency.</p>
              </div>
            </div>
          </div>
        </div>
    </section>
    <section class="call_to_action">
      <div class="container">
        <h3>We are extremely <strong>passionate</strong> and <strong>committed</strong> to <strong>creating strong bonds</strong> with <strong>our clients</strong>!</h3>
      </div>
    </section>
    <section class="team_members">
      <div class="container">
        <h2 class="section_header fancy centered">
            Meet Our Team
            <small style="width: 80%;">Our award-winning, multicultural creative team offers a unique advantage and sensibility that supports our environmental mission.</small>
          </h2>
        <div class="row">
          <div class="col-sm-6 col-md-4">
            <div class="team_member">
              <figure style="background-image: url(images/1b.jpg)"><img src="images/1a.jpg" alt="1a"></figure>
              <h5>Rick Shields</h5>
              <em>Presidenté</em>
              <hr>
              <div class="team_social">
                <a href="https://www.facebook.com/highmountaingraphics/" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="https://www.linkedin.com/company/high-mountain-graphics" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
                <a href="https://twitter.com/highmtngraphics" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="https://plus.google.com/+HighMountainGraphicsHawthorne/about" target="_blank"><i class="fa fa-google-plus"></i></a>
              </div>
              <p class="short_bio">A Graphic Arts veteran with over 30 years experience in typography, layout &amp; design, color-management, photo-retouching, prepress, paper and printing.</p>
            </div>
          </div>
          <!-- <div class="col-sm-6 col-md-4">
            <div class="team_member">
              <figure style="background-image: url(images/4b.jpg)"><img src="images/4a.jpg" alt="4a"></figure>
              <h5>Greg Barber</h5>
              <em>Print Sales</em>
              <hr>
              <div class="team_social">
                <a href="https://www.facebook.com/EcoFriendlyPrinter" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="https://www.linkedin.com/company/eco-friendly-printer" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
                <a href="https://twitter.com/greenprinter" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="https://plus.google.com/+HighMountainGraphicsHawthorne/about" target="_blank"><i class="fa fa-google-plus"></i></a>
              </div>
              <p class="short_bio">Press Technician with over 25 years of industry experience, from small to large format sheetfed printing and complete bindery.</p>
            </div>
          </div> -->
          <div class="col-sm-6 col-md-4">
            <div class="team_member">
              <figure style="background-image: url(images/2b.jpg)"><img src="images/2a.jpg" alt="2a"></figure>
              <h5>Terri A. Shields</h5>
              <em>Vice President</em>
              <hr>
              <div class="team_social">
                <a href="https://www.facebook.com/highmountaingraphics/" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="https://www.linkedin.com/company/high-mountain-graphics" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
                <a href="https://twitter.com/highmtngraphics" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="https://plus.google.com/+HighMountainGraphicsHawthorne/about" target="_blank"><i class="fa fa-google-plus"></i></a>
              </div>
              <p class="short_bio">Sales, administration and supervises day to day operations at High Mountain Graphics and manages the business overall.</p>
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="team_member">
              <figure style="background-image: url(images/3b.jpg)"><img src="images/3a.jpg" alt="3a"></figure>
              <h5>Rikki Lynn Shields</h5>
              <em>Jr. Art Director</em>
              <hr>
              <div class="team_social">
                <a href="https://www.facebook.com/highmountaingraphics/" target="_blank">
                  <i class="fa fa-facebook"></i>
                </a>
                <a href="https://www.linkedin.com/company/high-mountain-graphics" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
                <a href="https://twitter.com/highmtngraphics" target="_blank">
                  <i class="fa fa-twitter"></i>
                </a>
                <a href="https://plus.google.com/+HighMountainGraphicsHawthorne/about" target="_blank"><i class="fa fa-google-plus"></i></a>
              </div>
              <p class="short_bio">Current college student studying Communications and English with a minor in History. Freelance writer for <a href="http://Allwomenstalk.com" target="_blank">Allwomenstalk.com</a>.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <footer>
      <section class="twitter_feed_wrapper">
        <div class="container">
          <div class="row">
            <div class="twitter_feed_icon col-sm-1 col-md-1"><a href="#twitter"><i class="fa fa-twitter"></i></a></div>
            <div class="col-sm-11 col-md-11">
              <blockquote>
                <p>Rick is the very best! I am always in impossible situations but High Mountain Graphics makes all things printable possible. I adore them!</p>
                — S. Hackney-Robinson, <a href="http://meandthegirls.com/" target="_blank">meandthegirls.com</a>
                <br />
                <br />
                <a href="./testimonials.php" target="_blank">See All Testimonials</a>
              </blockquote>
            </div>
          </div>
        </div>
      </section>
      <?php echo $copyright; ?>
    </footer>
    </div>
    <?php echo $scripts; ?>
</body>

</html>
