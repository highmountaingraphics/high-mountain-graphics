<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
    <title>High Mountain Graphics - Environmental Printing</title>
    <?php echo $head; ?>

    <style media="screen">
      p ~ h5 {
        margin-bottom: 0;
      }

      h5 ~ table {
        margin-top: 15px;
      }
    </style>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" class="collapsing_header">
  <?php echo $header; ?>
    <div class="main">
      <section>
        <div class="paper-options container">
          <h3><strong>PAPER OPTIONS</strong></h3>
          <p>Visit <a href="http://brokencartons.com" target="_blank">BrokenCartons.com</a> for discounted Paper, Envelopes, and more…</p>
          <hr>
          <h3><strong>UNDERSTANDING YOUR CHOICES</strong></h3>
          <p>When you choose recycled paper, you have the power to help protect the environment and conserve natural resources. It is important to understand how fiber, water, and energy determine the overall environmental impact of the paper you choose.</p>

          <h5><strong>Our Specialty</strong></h5>
          <p>
            We specialize in obtaining spectacular offset printing results on certified recycled paper. Our house stocks are FSC, SFI and other certified, 100% post-consumer waste, certified processed chlorine free, recycled and tree-free paper.
          </p>

          <h5><strong>How to Choose Paper</strong></h5>
          When deciding on what type of paper to use for a particular project the following characteristics must be decided:</p>

          <h5><strong>Paper Type</strong></h5>
          <p>Paper is available in text, cover, and writing. Type of paper selected is determined by the end use of the printed piece. It is always best to discuss your paper needs at the start of your project.</p>

          <h5><strong>Color &amp; Brightness</strong></h5>
          <p>Paper is available in a wide variety of colors and brightness. Our environmental paper inventory includes brightness ranging up to 96% which brings a new level of brightness and whiteness to the recycled paper market.</p>

          <h5><strong>Size</strong></h5>
          <p>Since many sizes are available in paper, we will recommend what paper sizes print best.</p>

          <h5><strong>Finish</strong></h5>
          <p>Paper is available in a variety of textures or finishes:</p>
          <ul>
            <li><strong>Antique:</strong> Rough, natural-looking finish used on uncoated papers, especially for books.</li>
            <li><strong>Gloss:</strong> High-gloss finish with bright reflection used on coated papers.</li>
            <li><strong>Smooth:</strong> Smooth, soft finish used on uncoated papers.</li>
            <li><strong>Matte:</strong> A non-reflecting finish used on coated papers.</li>
            <li><strong>Linen:</strong> Honeycomb finish reminiscent of canvas, used on uncoated papers.</li>
            <li><strong>Laid:</strong> Grids of embossed parallel lines in a specific pattern, used on uncoated papers.</li>
            <li><strong>Vellum:</strong> Relatively rough with a natural appearance that resembles an eggshell, used on uncoated papers.</li>
          </ul>
          <h5><strong>Text Paper</strong></h5>
          <p>A paper of fine quality manufactured in white and colors and made in a variety of finishes such as felt, linen, laid, smooth, and vellum. Text paper is simply the paper that is traditionally used between the covers of a book or pamphlet. Standard text weights are 50, 60, 70, 80, and 100 lb.</p>

          <h5><strong>Cover Paper</strong></h5>
          <p>A paper that is more durable and thicker than text paper manufactured in white and colors along with a variety of finishes such as, vellum, smooth, felt, laid and embossed. Cover paper is used for business cards, brochures and as covers for books, catalogs, and pamphlets. Standard cover weights are 65, 80, 100 lb and above.</p>

          <h5><strong>Writing or Bond Paper</strong></h5>
          <p>A paper of fine quality manufactured in white and colors and made in a variety of finishes such as, vellum, smooth, felt, laid and embossed. Writing paper is used primarily for company stationery and correspondence. Standard writing weights are 20, 24, and 28 lb.</p>

          <h5><strong>Basis Weight</strong></h5>
          <p>Paper comes in several different basis weights (see below). The basis weight is usually determined by the end use of the printed piece. All text, cover, and writing paper is divided into basis weights. The heavier the basis weight per type of paper, the thicker and heavier the paper. It must be noted that each type of paper (text, cover, writing) must be looked at individually in terms of the basis weight. For example, 100 lb text is not as thick or heavy as 100 lb cover, but 100 lb text is thicker and heavier than 60 lb text. For basis weight comparisons text should always be compared with text, cover with cover, and writing with writing.</p>

          <h5><strong>Equivalent Weights in Paper</strong></h5>
          <p>Use the table below to compare one type of paper to another for basis weight. Find the weight under the heading and the approximate equivalent basis weights for the other types of paper will be on the right or left.</p>

          <h5><strong>Example:</strong> A 24 lb writing or bond is equivalent to a 60 lb text and a 33 lb. cover.</h5>
          <h5><strong>Example:</strong> A 70 lb text is equivalent to a 28 lb writing and a 39 lb cover.</h5>
          <h5><strong>Example:</strong> A 65 lb cover is equivalent to a 120 lb text or a 46 lb writing.</h5>

          <table>
            <tr>
              <th>
                Writing &amp;
                <br>
                Bond
              </th>
              <th>Text</th>
              <th>Cover</th>
            </tr>
            <tr>
              <td>16 lb</td>
              <td>40 lb</td>
              <td>22 lb</td>
            </tr>
            <tr>
              <td>20 lb</td>
              <td>50 lb</td>
              <td>28 lb</td>
            </tr>
            <tr>
              <td>24 lb</td>
              <td>60 lb</td>
              <td>33 lb</td>
            </tr>
            <tr>
              <td>28 lb</td>
              <td>70 lb</td>
              <td>39 lb</td>
            </tr>
            <tr>
              <td>32 lb</td>
              <td>80 lb</td>
              <td>45 lb</td>
            </tr>
            <tr>
              <td>36 lb</td>
              <td>90 lb</td>
              <td>50 lb</td>
            </tr>
            <tr>
              <td>38 lb</td>
              <td>100 lb</td>
              <td>55 lb</td>
            </tr>
            <tr>
              <td>46 lb</td>
              <td>120 lb</td>
              <td>65 lb</td>
            </tr>
            <tr>
              <td>58 lb</td>
              <td>145 lb</td>
              <td>80 lb</td>
            </tr>
            <tr>
              <td>72 lb</td>
              <td>183 lb</td>
              <td>100 lb</td>
            </tr>
          </table>

          <hr>
          <h3><strong>PAPER MILLS</strong></h3>

          <h5><strong>Appvion Papers</strong>, Appleton, WI USA</h5>
          <p>Appvion (formerly Appleton Papers) is an employee owned paper mill located in the Paper Valley region of Wisconsin in the city of Appleton. For generations Appvion has created some of the most interesting and dynamic specialty papers in the market place. As a specialty coating facility Appvion makes a variety of paper for graphics applications and industrial use.</p>

          <h5><strong>Arjo Wiggins</strong>, Boulogne Billancourt Cedex, FRANCE</h5>
          <p>ARJOWIGGINS is one of the world's great paper making companies. It's history dates back to 1492 starting with the creation of the Arches paper mill in France, today ARJOWIGGINS has paper mills and distribution worldwide. Creative papermaking is an everyday event at ARJOWIGGINS and has resulted in the creation of the Curious Collection of fine papers including premium coated, translucent, metallic and other unique and creative papers.
          </p>

          <h5><strong>Astro Converters</strong>, San Marcos, CA USA</h5>
          <p>Astro Converters is one of the premier creators of unique paper lines. Astro is a privately owned family company based in California. For more than 40 years Astro Converters has been producing an increasingly interesting collection of specialty note cards, invitations, envelopes and papers. With an unusual sense of style and an eye for the best papers and cardstock, Astro offers an unmatched selection of specialty paper products.</p>

          <h5><strong>Avery Dennison Corporation</strong>, Pasadena, CA USA</h5>
          <p>Avery Dennison Corporation, Founded in 1935, is a global manufacturer and distributor of pressure-sensitive adhesive materials, office products, and various paper products.</p>

          <h5><strong>Beckett Paper</strong></h5>
          <p>Beckett Concept is now part of part of Strathmore Premium, Wove finish. Some shades have been discontinued but inventory may still be available. Please check the Product Finder to search for available inventory of legacy items.</p>

          <h5><strong>BrokenCartons</strong></h5>
          <p>Looking for small quantities of paper and envelopes? Want to turn excess inventory into profit? <a href="https://brokencartons.com" target="_blank">BrokenCartons.com</a> is an e-commerce marketplace that helps members BUY &amp; SELL discounted paper related products using our co-operative platform. <a href="https://www.brokencartons.com/category.jhtm?cid=90" target="_blank">JOIN NOW!</a> It’s FREE and EASY…</p>

          <h5><strong>Cascades Fine Papers group</strong>, Saint-Jérôme (QC)</h5>
          <p>Cascades Fine Papers Group, is a leader in the manufacturing of ecological fine papers and security papers in Canada. The Group uses 50% and 100% post-consumer fiber to manufacture its commercial papers and uses renewable and local biogas energy that is produced from the decomposition of waste buried in landfills.</p>

          <h5><strong>Crane &amp; Co</strong>, Dalton, MA USA</h5>
          <p>A family owned company since 1770 Crane &amp; Co. When you purchase Crane stationery you're in good company with other famous Crane &amp; Co. customers such as Paul Revere, Eleanor Roosevelt and the Queen of England. All Crane &amp; Co papers are made from 100% cotton papers and printed by old world craftsmen in it's in-house pressroom using letterpress, foiling, engraving and offset printing to add the ultimate in elegance and quality to every piece. Custom stationery is made to order with your choice of font and colors that you'll easily design online.</p>

          <h5><strong>CTI Paper USA</strong>, Sun Prairie, WI USA</h5>
          <p>CTI Paper USA offers an eclectic ensemble of papers designed to inspire creative genius. Elegant, sophisticated, fresh and fun — The Fine Paper Collection includes the world's finest translucent, cast coated and metallic printing papers available. CTI Paper USA uses wind power in all of our facilities. We choose to purchase wind power for our corporate offices as well as our converting and distribution center to help protect the environment and reduce greenhouse gasses. CTI Paper USA is a privately owned company established in 1989.</p>

          <h5><strong>Domtar</strong>, Montreal, Canada</h5>
          <p>As the largest integrated paper mill and marketer of uncoated fine paper in North America and the second largest in the world, based on capacity. Domtar makes pulp and paper and has 10 pulp and paper mills in operation (8 in the United States and 2 in Canada), with an annual paper production capacity of approximately 3.8 million tons of uncoated fine paper. Approximately 81% of our paper production capacity is located in the USA and the remaining 19% is located in Canada. Domtar paper manufacturing operations are supported by 15 converting and distribution operations, including a network of 11 plants located offsite of our paper mill operations.</p>

          <h5><strong>Finch Paper</strong>, Glens Falls, NY USA</h5>
          <p>Located in the foothills of the Adirondack Mountains Finch Paper, llc manufactures more than 250,000 tons per year of premium uncoated printing papers under the brand names Finch Fine Text &amp; Cover paper, Finch Digital and Finch Opaque. Finch Paper recently preserved its 120,000 acres of Adirondack forest lands for all time through a sale to the Nature Conservancy.</p>

          <h5><strong>Fraser Papers</strong>, Toronto, Ontario CA</h5>
          <p>Fraser Papers Inc. was a Toronto, Ontario, Canada-based manufacturer of specialized printing, publishing, and converting papers, with customers in Canada and the US.</p>

          <h5><strong>French Paper</strong>, Niles, Michigan</h5>
          <p>Established in 1871, French Paper is a sixth-generation, family-owned American company. French Paper Company has been manufacturing paper for more than 140 years in the same community of Niles, Michigan. In an industry known for corporate acquisitions and shutdowns, French Paper has persevered, emerging as one of the strongest, smartest, and most consistent paper brands around.</p>

          <h5><strong>Fox River</strong>, Neenah, WI USA</h5>
          <p>FOX RIVER Select is cunningly created with Writing and Cover weights in the softest shades that allow business cards, presentation folders announcements to work together—even in the most complex identity applications. FOX RIVER is now under the Neenah Paper Mill.</p>

          <h5><strong>GMUND</strong>, Gmund Germany</h5>
          <p>Papierfabrik GMUND is a family owned paper mill located in the Bavarian region of southern Germany in the small town of GMUND. The Kohler family has overseen the creation of some of the world's finest paper design and manage the GMUND paper mill which has been in operation since 1829. The craftsmanship of the papers produced by GMUND are like no others offered in the market today.</p>

          <h5><strong>Gruppo Cordenons Papers</strong>, Milano, Italia</h5>
          <p>The Cordenons mill continued its reputation of producing the highest quality fine paper for centuries when, in 1984, the mill was purchased by its current owners, the Gilberti family. The Gilberti family completely renovated the mill to its current state-of-the-art facility, committed to the tradition of producing the highest-quality fine papers available.</p>

          <h5><strong>International Paper</strong>, Memphis, TN USA</h5>
          <p>International Paper Company is an American pulp and paper company, the largest such company in the world. International Paper is a global leader in the paper and packaging industry with manufacturing operations in North America, Europe, Latin America, Russia, Asia and North Africa.</p>

          <h5><strong>Leader Paper Products</strong>, Milwaukee, WI USA</h5>
          <p>Leader is a family owned business focusing on the manufacture of envelopes and other specialty paper products. Being a leader in specialty paper finished products Leader creates and markets interesting high-end paper product for use in all fine paper projects including crafting and printing projects.</p>

          <h5><strong>Legion Paper</strong>, New York, NY USA</h5>
          <p>Legion Paper is the leading supplier of fine art and specialty papers from the world over. Focusing on European specialty mills Legion Paper consistently finds and makes available some of the most exciting and luxurious papers available anywhere.</p>

          <h5><strong>MACtac</strong>, Stowe, OH USA</h5>
          <p>MACtac ® is a leading manufacturer of pressure sensitive adhesive (self-adhesive) products worldwide. Along with a strong presence and base in North America, MACtac is a worldwide supplier serving important markets in Europe, South America and Asia and has manufacturing and distribution facilities in 14 countries worldwide. MACtac's international presence reflects the global strength and innovation of its parent, the Bemis Company, the largest flexible packaging supplier in North America. MACtac's products are used in a wide range of industries including printing, graphic design, packaging, digital imaging, photographic, assembly engineering, communications and medical.</p>

          <h5><strong>Mohawk Fine Papers</strong>, Cohoes, NY USA</h5>
          <p>Mohawk Fine Papers is a women owned family business managed by the O'Connor family. In 2005, Mohawk paper company acquired the Fine Papers business from International Paper, which included the respected Strathmore, Beckett, Via, and BriteHue brands. Today Mohawk Fine Papers operates three mills (six paper machines) with over 175,000 ton capacity. The mills are served by two Converting Centers, which offer state-of-the-art cut-size, folio, roll converting, and packaging to paper distributors, paper mills, and OEMs.</p>

          <h5><strong>Monadnock Paper Mills</strong>, Bennington, NH USA</h5>
          <p>Monadnock Paper Mills is an independent, family-owned paper company making paper since 1819. It is the oldest, continuously operating paper-manufacturing site in America. Today, Monadnock Paper Mills applies uncompromising standards and rigorous discipline to elevate the level of craftsmanship and performance in specialty and fine paper manufacturing. Located in the pristine forest area of southern New Hampshire close to it's namesake Mt. Monadnock, the company has been a leader in environmentally sound manufacturing processes for many years and has recently achieved the Environmental Certification: ISO 14001:2004.</p>

          <h5><strong>Neenah Paper Company</strong>, Neenah, WI USA</h5>
          <p>Neenah Paper manufactures and distributes a wide range of premium and specialty paper grades, with leading positions in many of its markets and well-known brands such as CLASSIC®, ENVIRONMENT®, UV/ULTRA® II, NEENAH®, EAMES ™ and other fine brand names. Neenah Paper is a proud consumer of Green Energy, and is one of the first paper mills to utilize "steam" energy that was converted using paper mill byproducts. Neenah also manufactures many brands carrying the Green Seal and Forest Stewardship Council (FSC) certifications.</p>

          <h5><strong>New Leaf Paper</strong>, Oakland, CA USA</h5>
          <p>New Leaf Paper Company is a leading producer of eco-preferable papers designed specifically to meet the needs of today's printers and consumers seeking to use premium papers with the lowest environmental impact without sacrificing cost and quality. Started in 1998 New Leaf has forged a growing number of dedicated customers who specify New Leaf products as the most trusted name in eco-preferable printing papers.</p>

          <h5><strong>NewPage</strong>, ‎Miamisburg, Ohio USA</h5>
          <p>NewPage, established in May 2005, is the leading producer of printing and specialty papers in North America. Our company's product portfolio is one of the broadest in the paper industry. We operate paper mills located in Kentucky, Maine, Maryland, Michigan, Minnesota and Wisconsin. With NewPage, you can be confident you are working with the most knowledgeable paper manufacturers in North America. Headquartered in Miamisburg, Ohio, we have more than 150 years of expertise in paper making. We are committed to delivering innovative products and services while being socially, environmentally and economically responsible.</p>

          <h5><strong>Pinnacle Label Mfg Inc</strong>, Buffalo, NY</h5>
          <p>Pinnacle Label has pioneered its Clean Edge technology that places an 1/8 inch border of non adhesive area around the outer border of the labels making these labels non-oozing and perfect for digital processes. As well, Pinnacle has the only Forest Stewardship Council (FSC) certified labels using 100% recycled face stock while providing fully recyclable labels. Because Pinnacle uses water-based adhesives you can confidently recycle these labels.</p>

          <h5><strong>grupo Portucel Soporcel</strong>, Portugal</h5>
          <p>A major player on the international paper and paper pulp market, the Portucel Soporcel group is today the European leader in the production of premium office paper, having gained this position thanks to the start-up of the new Setúbal mill.</p>

          <h5><strong>Reich Paper</strong>, Brooklyn, NY</h5>
          <p>Reich Paper has been offering specialty papers to designers, printers, and connoisseurs of fine paper for over 50 years. Founded in 1958 by Daniel O. Reich, the company today is international in outlook, but remains a small, family-owned and customer-focused business.</p>

          <h5><strong>Roosevelt Paper Company</strong>, Mt Laurel, NJ</h5>
          <p>One of America's leading paper distributors and converters, Roosevelt offers a time tested combination of selection, service and savings that have made us a prime resource for the printing, publishing and packaging industries, both large and small, across the nation and abroad.</p>

          <h5><strong>Sappi</strong>, Braamfontein, South Africa</h5>
          <p>Sappi Fine Paper is a leading global paper company specializing in coated fine paper and sustainability. Learn how this paper mill produces the highest quality.</p>

          <h5><strong>Papierfabrik Schoellershammer</strong>, GERMANY</h5>
          <p>Papierfabrik Schoellershammer (pronounced Schoeller's hammer) has produced fine papers since 1784 and today is recognized as one of Europe's leading masters of artist paper manufacturing.</p>

          <h5><strong>Southworth Paper</strong>, Agawam, MA</h5>
          <p>Southworth Paper is a privately owned family company in business for more than 100 years and is the leading supplier of fine stationery, resume paper and cotton content papers. In it's New England paper mill the company produces premium papers in one of the oldest operating paper mills in the country. When quality and economy are important Southworth Paper offers the best solutions.</p>

          <h5><strong>Stora Enso</strong>, Helsinki, Finland</h5>
          <p>Stora Enso is the global rethinker of the paper, biomaterials, wood products and packaging industry. We always rethink the old and expand to the new to offer our customers innovative solutions based on renewable materials.</p>

          <h5><strong>Strathmore Artist</strong></h5>
          <p>Strathmore Artist Papers offers a broad array of specialty papers specializing in art papers and digital reproduction.</p>

          <h5><strong>Superior Specialties</strong>, Neenah, WI</h5>
          <p>Superior Specialties offers some of the best quality inkjet papers for photographers.The company is privately owned specializes in unique paper products for consumers and professionals.</p>

          <h5><strong>Wausau Paper</strong> Wausau, WI USA</h5>
          <p>With more than 100 years of experience, Wausau Paper is one of the industry's leading producers of fine paper and related products. Wausau Paper has always been a leader in colored paper products innovation. The Astrobrights ® line of bright-colored papers revolutionized the industry. Today, they continue to be widely known as the experts in colored paper.</p>

          <h5><strong>WorldWin Papers</strong> Sun Prairie, WI USA</h5>
          <p>WorldWin Papers Craft and scrapbook products are specifically designed for paper crafters and for resale through quality craft stores. Based in the heart of Wisconsin's paper-making region, you'll enjoy the WorldWin advantage.</p>

          <h5><strong>Xerox</strong>, Norwalk, CT USA</h5>
          <p>Xerox Corporation is an American multinational document management corporation that produces and sells a range of color and black-and-white printers, multifunction systems, photo copiers, digital production printing presses, and related consulting services and supplies.</p>

          <h5><strong>YUPO Synthetic Paper</strong>, Chesapeake, VA USA</h5>
          <p>YUPO Synthetic Paper is 100% recyclable, waterproof and tree-free, with attributes and properties that make it the perfect solution for a variety of marketing, design, packaging and labeling needs.</p>

          <h5><strong>COPYRIGHT</strong></h5>
          <p>All trademarks remain property of their respective holders, and are used only to directly describe the products being provided. Their use in no way indicates any relationship between the holders of said trademarks. All other content is copyright 2016 High Mountain Graphics, llc. All Rights Reserved.</p>
        </div>
      </section>
      <section class="clients_section wow animated fadeInUp">
        <div class="container">
          <h2 class="section_header skincolored centered">PAPER MILLS</h2>
          <div class="clients_list">
            <a href="http://www.domtar.com/" target="_blank"><img src="images/products/domtar.jpg" alt="domtar"></a>
            <a href="http://www.finchpaper.com/" target="_blank"><img src="images/products/finch.jpg" alt="finch"></a>
            <a href="https://www.mohawkconnects.com/" target="_blank"><img src="images/products/mohawk.jpg" alt="mohawk"></a>
            <a href="http://mpm.com/" target="_blank"><img src="images/products/monadnock.jpg" alt="monadnock"></a>
            <a href="http://www.neenahpaper.com/" target="_blank"><img src="images/products/neenah.jpg" alt="neenah"></a>
          </div>
        </div>
      </section>
      <footer>
          <?php echo $copyright; ?>
      </footer>
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script>
      window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')
    </script>
    <?php echo $scripts; ?>
</body>

</html>
