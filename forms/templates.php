<?php
include_once("../analyticstracking.php");

$header = '
<header>
  <div class="container">
    <div class="navbar navbar-default" role="navigation">
      <div class="navbar-header">
        <a href="../../">
          <img src="../../images/HMG.jpg" alt="High Mountain Graphics">
        </a>
        <a class="btn btn-navbar btn-default navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="nb_left pull-left">
							<span class="fa fa-reorder"></span>
          </span>
          <span class="nb_right pull-right">menu</span>
        </a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav pull-right navbar-nav">
          <li><a href="../../">Home</a></li>
          <li><a href="../../about-us.php">About Us</a></li>
          <li><a href="../../products.php">Products</a></li>
          <li><a href="../../services.php">Services</a></li>
          <li><a href="../../portfolio.php">Portfolio</a></li>
          <li class="dropdown">
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          		Resources
          		<span class="caret"></span>
        		</a>
            <ul class="dropdown-menu">
              <li><a href="../../prepress-guidelines.php">Prepress Guidelines</a></li>
              <li><a href="../../terms-conditions.php">Terms & Conditions</a></li>
              <li><a href="../../environmental-policies.php">Environmental Policies</a></li>
              <li><a href="../../environmental-associations.php">Environmental Associations</a></li>
              <li><a href="../../paper-options.php">Paper Options</a></li>
            </ul>
          </li>
          <li><a href="../../contact.php">Contact</a></li>
        </ul>
      </div>
    </div>
    <div id="sign">
      <a href="../estimate/form.php"><span>Request an Estimate!</span></a>
      <a href="../order/form.php"><span>Place an Order!</span></a>
    </div>
    <div id="social_media_wrapper">
      <a href="https://www.facebook.com/highmountaingraphics" target="_blank">
        <img src="../../images/index/social/facebook.png" alt="facebook"/>
      </a>
      <a href="https://twitter.com/highmtngraphics" target="_blank">
        <img src="../../images/index/social/twitter.png" alt="twitter"/>
      </a>
      <a href="https://plus.google.com/+HighMountainGraphicsHawthorne/about" target="_blank">
        <img src="../../images/index/social/google-plus.png" alt="google plus"/>
      </a>
      <a href="https://www.linkedin.com/company/high-mountain-graphics" target="_blank">
        <img src="../../images/index/social/linkedin.png" alt="linkedin"/>
      </a>
      <a href="http://brokencartons.com/blog" target="_blank">
        <img src="../../images/index/social/blogger.png" alt="blog"/>
      </a>
      <a href="https://www.youtube.com/user/highmountaingraphics" target="_blank">
        <img src="../../images/index/social/youtube.png" alt="youtube"/>
      </a>
      <a href="http://www.yelp.com/biz/high-mountain-graphics-north-haledon" target="_blank">
        <img src="../../images/index/social/yelp.png" alt="yelp"/>
      </a>
    </div>
  </div>
</header>
';

$copyright = '
<section class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-6">© <span id="copyright-year">1995 - <script>document.write(new Date().getFullYear())</script></span> High Mountain Graphics, llc. All rights reserved.</div>
			<div class="text-right col-sm-6 col-md-6"> 5 Sicomac Road, Suite 124 · North Haledon · N.J. 07508 (973) 427-5820</div>
		</div>
	</div>
</section>
';
