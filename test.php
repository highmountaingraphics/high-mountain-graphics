<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
  <title>Prepress Guidelines</title>
  <?php echo $head; ?>

  <style>
    .formFooter {
      display: none !important;
    }
  </style>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" class="collapsing_header">
  <?php echo $header; ?>
  <div class="main">
    <section>
      <div class="container">
        <script type="text/javascript" src="https://form.jotform.us/jsform/72308012383145"></script>
      </div>
    </section>
    <footer>
      <?php echo $copyright; ?>
    </footer>
  </div>
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script>
    window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')
  </script>
  <?php echo $scripts; ?>
</body>

</html>
