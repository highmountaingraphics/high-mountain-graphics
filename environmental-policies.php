<!doctype html>
<html class="no-js" lang="en">

<head>
<?php include "templates.php"; ?>
    <title>Environmental Policies</title>
    <?php echo $head; ?>

    <style media="screen">
      p ~ h5 {
        margin-bottom: 0;
      }

      ul {
        margin-top: 5px;
      }
    </style>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" class="collapsing_header">
  <?php echo $header; ?>
  <div class="main">
    <section>
      <div class="container">
        <h3><strong>ENVIRONMENTAL POLICIES</strong></h3>
        <p>The issues aren't black and white…They're GREEN!</p>

        <h5><strong>Paper</strong></h5>
        <ul>
          <li>We actively encourage and promote the use of FSC, SFI and other environmentally certified paper.</li>
          <li>We promote 100% Post Consumer, Certified Processed Chlorine Free, Tree Free & Recycled Paper.</li>
          <li>Tree Free fibers include Bamboo, Hemp, Sugarcane Bagasse, Mango, Lemon, Coffee and more.</li>
        </ul>

        <h5><strong>Inks & Toners</strong></h5>
        <ul>
          <li>We use vegetable based inks consisting of Soya, Chinawood and Linseed which are all renewable products.</li>
          <li>Our inks are manufactured locally, without lead, cadmium, mercury, or hexavalent chromium compounds.</li>
          <li>We specialize is Pantone® spot color printing which uses significantly less paper, ink, solvents, and energy.</li>
          <li>Our full color digital printing with non-toxic toner reduces waste and avoids the solvents used in offset printing.</li>
        </ul>

        <h5><strong>Solvents</strong></h5>
        <ul>
          <li>We use low and no voc (volatile organic compounds) solvents for all of our printing and production requirements.</li>
          <li>Our printing plates, rags and chemicals are recycled, washed or disposed of according to EPA regulations.</li>
          <li>We use water based aqueous coatings and varnishes on products which require a protective sealant.</li>
        </ul>

        <h5><strong>Production</strong></h5>
        <ul>
          <li>We develop products, processes, and working methods to reduce the waste of paper, water, and energy.</li>
          <li>We segregate, reduce, reuse and recycle waste material unavoidably produced in the production processes.</li>
          <li>Our direct-to-plate technology eliminates the need for film used in traditional methods of commercial printing.</li>
        </ul>

        <h5><strong>Legislation Compliance</strong></h5>
        <ul>
          <li>We are fully compliant with the Hazardous Waste Act and waste is segregated for recycling by licensed contractors.</li>
          <li>We are assessed regularly by independent auditors to ensure we are managing environmentally friendly practices.</li>
        </ul>

        <h5><strong>Cooperative & Non Profit Programs</strong></h5>
        <ul>
          <li>We encourage gang run printing which reduces production waste by up to 95% while delivering cost savings.</li>
          <li>We discount costs for non-profit organizations choosing our environmentally friendly graphics and printing services.</li>
          <li>Our buying power enables us to purchase eco-friendly paper and materials at a lower cost and pass on the savings.</li>
          <li>Investment in the latest technology and enables High Mountain Graphics to remain highly competitive and efficient.</li>
          <li>Online ordering, proofing and billing reduces production costs and savings are passed onto you, our customer.</li>
        </ul>

        <h5><strong>Our Commitment to the Environment</strong></h5>
        <p>Through endless hours of research we continue to educate ourselves, our staff, our clients, and vendors to improve on our goal of providing environmentally friendly graphics, printing, and sign solutions.</p>

        <h5><strong>Need More Information?</strong></h5>
        <p>Call us at (973) 427-5820 for additional information on environmentally friendly paper, printing, marketing and sign solutions.</p>
      </div>
    </section>
    <footer>
        <?php echo $copyright; ?>
    </footer>
  </div>
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script>
    window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')
  </script>
  <?php echo $scripts; ?>
</body>

</html>
