<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
  <title>Environmental Associations</title>
  <?php echo $head; ?>

  <style media="screen">
    p ~ h5 {
      margin-bottom: 0;
    }
  </style>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" class="collapsing_header">
  <?php echo $header; ?>
  <div class="main">
    <section>
      <div class="container">
        <h3><strong>ENVIRONMENTAL ASSOCIATIONS </strong></h3>
        <p>Once is not enough…Reduce, Reuse, and Recycle!</p>

        <h5><strong>Abundant Forests Alliance</strong> (AFA)</h5>
        <p>Members of the wood and paper products industry in North America have joined together to form the Abundant Forests Alliance. The group advocates sustainable forest practices, such as harvesting, replanting, as well as recycling. They're a group with many goals but one mission — to reassure you that there will always be plenty of forests.</p>

        <h5><strong>American Forest &amp; Paper Association</strong> (AF&amp;PA)</h5>
        <p>The American Forest &amp; Paper Association is the national trade association of the forest, pulp, paper, paperboard, and wood products industry. They represent member companies engaged in growing, harvesting, and processing wood and wood fiber, manufacturing pulp, paper, and paperboard products from both virgin and recycled fiber, and producing engineered and traditional wood products. AF&amp;PA's mission is to influence successfully public policy to benefit the U.S paper and forest products industry.</p>

        <h5><strong>Carbon Neutral (CN)</strong></h5>
        <p>Carbon neutral is an emerging trend for both companies and individuals around the world. It can describe products, operations and activities which have had their carbon dioxide emissions: 1) calculated; 2) reduced where possible; 3) "offset" through credits that fund additive, emission-free energy projects such as wind farms and solar installations. By purchasing these credits, organizations can apply them to their own emissions and reduce their net carbon impact. The end result is termed carbon neutral.</p>

        <h5><strong>Chlorine Free Products Association</strong> (CFPA)</h5>
        <p>The Chlorine Free Products Association is an independent not-for-profit accreditation and standard setting organization, incorporated in the state of Illinois. The primary purpose of the association is to promote Total Chlorine Free policies, programs, and technologies throughout the world.</p>

        <h5><strong>EPA Green Power Partnership</strong></h5>
        <p>This partnership recognizes paper mill's use of wind-power to manufacture pcw papers as part of best-practices environmental management. Green power is a subset of renewable energy and represents those renewable energy resources and technologies that provide the highest environmental benefit. EPA defines green power as electricity produced from solar, wind, geothermal, biogas, biomass, and low-impact small hydroelectric sources. Customers often buy green power for avoided environmental impacts and its greenhouse gas reduction benefits.</p>

        <h5><strong>Forest Stewardship Council</strong> (FSC)</h5>
        <p>The Forest Stewardship Council is an international organization that brings people together to find solutions which promote responsible stewardship of the world’s forests. Their goal is to shift the market to eliminate habitat destruction, water pollution, and displacement of indigenous peoples. FSC Recycled certifies the 100% post-consumer content of a product, taking into account all transformation steps, from raw material to consumer product.</p>

        <h5><strong>Green-e</strong></h5>
        <p>The Green-e logo Identifies products made with certified renewable energy, including but not limited to wind power, solar power, low impact hydropower, and biomass. The Green-e Program certifies that the wind-generated electricity paper mill's purchases meets strict environmental and consumer protection standards established by the nonprofit Center for Resource Solutions.</p>

        <h5><strong>Green Seal</strong></h5>
        <p>Green Seal is an independent non-profit organization dedicated to safeguarding the environment and transforming the marketplace by promoting the manufacture, purchase, and use of environmentally responsible products and services. Green Seal Certification ensures that a product meets rigorous, science-based environmental leadership standards. This gives manufacturers the assurance to back up their claims and purchasers confidence that certified products are better for human health and the environment.</p>

        <h5><strong>Mohawk Windpower</strong></h5>
        <p>Mohawk uses this logo to identify papers manufactured with wind-generated electricity. Projects printed on any of these papers may include this logo with approval from Mohawk Fine Papers.</p>

        <h5><strong>Processed Chlorine Free</strong> (PCF)</h5>
        <p>Certification from the Chlorine Free Product Association - CFPA. This certification ensures the delivery of premium papers that are manufactured free of chlorine chemistry and from sustainable raw materials.</p>

        <h5><strong>Rainforest Alliance Endorsed</strong></h5>
        <p>The alliance's goals are to manage the delicate balance between supplying wood and paper products and maintaining healthy abundant forests and protecting trees, soil, air, water, and wildlife habitats. The Rainforest Alliance mission works to conserve biodiversity and ensure sustainable livelihoods by transforming land-use practices, business practices and consumer behavior.</p>

        <h5><strong>Recycle Logo</strong></h5>
        <p>This indicates that a product or package is recyclable and/or made with recycled materials. The Federal Trade Commission (FTC) and the International Organization for Standardization (ISO) set up guidelines for the use of the recycled symbol as well as the use of broader marketing claims in an effort to ensure the recycling symbol is used appropriately and marketing claims are substantiated. You may use the recycled symbol to identify products that are recyclable, products that contain 100% recycled content, and products that contain a portion of recycled content. Used alone with no descriptive text, the symbol indicates that a product is both recyclable and contains recycled content. The symbol must be accompanied by qualifying statements to ensure the claim is accurate.</p>

        <h5><strong>SmartWood</strong></h5>
        <p>A program of The Rainforest Alliance.</p>

        <h5><strong>Soy Ink</strong></h5>
        <p>The center was created and funded by the Iowa Soybean Association (ISA) using soybean checkoff dollars. Iowa farmers believed in soy ink and committed many resources to promoting its usage. Now that it is a success, ISA is moving those resources to fund exciting new innovations and programs that will build new markets for our soybeans. ISA is making this change to the National Soy Ink Information Center because there is now a general awareness and acceptance of soy ink worldwide. People understand and value it as a renewable resource and alternative to petroleum-based inks.</p>

        <h5><strong>Sustainable Forestry Initiative</strong> (SFI)</h5>
        <p>The Sustainable Forestry Initiative program is based on the premise that responsible environmental behavior and sound business decisions can coexist. This unique commitment to sustainable forestry recognizes that all forest landowners, not just SFI program participants, play a critical role in ensuring the long-term health and sustainability of our forests.</p>

        <h5><strong>Communicating Recycled Content</strong></h5>
        <p>Recycled content is often expressed in an equation denoting its percentage of content. It is generally labeled with the total amount of recycled content first and the amount of post-consumer material second.</p>

        <h5><strong>Examples of Use:</strong></h5>
        <p>Printed on 100% Recycled Paper using Soy Ink.</p>
        <p>Printed on 100% recycled paper containing 30% post-consumer fiber.</p>
        <p>Printed on 100% post-consumer, processed chlorine free, recycled paper using vegetable based soy ink.</p>
        <p>Printed Digitally on 100% post-consumer, processed chlorine free, recycled paper using non-toxic toners.</p>

        <p>We offer a 5%-10% courtesy discount for adding our tagline: <strong>Printing with the environment in mind: www.highmountaingraphics.com</strong>.</p>

        <h5><strong>Logo Use</strong></h5>
        <p>To obtain the appropriate logo(s) and use, call us at (973) 427-5820 or email info@highmountaingraphics.com with specific project information and the logo you’re requesting.</p>

      </div>
    </section>
    <section class="clients_section wow animated fadeInUp">
      <div class="container">
        <h2 class="section_header skincolored centered">ENVIRONMENTAL ASSOCIATIONS</h2>
        <div class="clients_list">
          <a href="http://www.afandpa.org/" target="_blank"><img src="images/services/certs/afapp.jpg" alt="afapp"></a>
          <a href="http://www.carbonneutral.com/" target="_blank"><img src="images/services/certs/carbon_neutral.jpg" alt="carbon_neutral"></a>
          <a href="https://us.fsc.org/en-us" target="_blank"><img src="images/services/certs/fsc.jpg" alt="fsc"></a>
          <a href="http://www.green-e.org/" target="_blank"><img src="images/services/certs/green_e.jpg" alt="green_e"></a>
          <a href="http://www.rainforest-alliance.org/" target="_blank"><img src="images/services/certs/rainforest_alliance.jpg" alt="rainforest_alliance"></a>
          <a href="http://www.sfiprogram.org/" target="_blank"><img src="images/services/certs/sfi.jpg" alt="sfi"></a>
        </div>
      </div>
    </section>
    <footer>
      <?php echo $copyright; ?>
    </footer>
  </div>
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script>
    window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')
  </script>
  <?php echo $scripts; ?>
</body>

</html>
