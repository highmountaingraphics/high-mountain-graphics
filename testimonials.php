<!doctype html>
<html class="no-js" lang="en">

<head>
<?php include "templates.php"; ?>
    <title>High Mountain Graphics - Environmental Printing</title>
    <?php echo $head; ?>

    <style>
      blockquote img {
        width: 10%;
      }

      blockquote {
        border-left: 0;
      }
    </style>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" class="collapsing_header">
  <?php echo $header; ?>
  <div class="main">
    <section>
      <div class="container">
        <h3><strong>Why brag when your customers do it for you!</strong></h3>

        <blockquote>
          <p>Rick takes great pride in the results of his work.  He is not only meticulous when it comes to color matching and detail, he goes out of his way to be sure his customer is satisfied.  He understood what I was looking for when I showed him my new brand and icon.  It was critical to me that my business cards and stationery captured the personality of my business and quality of my work, and Rick made sure that what I got was nothing short of fabulous. Thanks, Rick, for bringing my new brand to life!</p>
          &mdash; Lisa M. Ference, DMD (<a target="_blank" href="http://www.cliftondentalarts.com/">Clifton Dental Arts</a>) Oct 3, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick and his crew a great! The orders come in very fast and a more than fair price. What else could we ask for?</p>
          &mdash; Peter Pavlov Oct 16, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>I've been working with Rick and High Mountain for about 10 years now. I have a design studio in NYC and High Mountain is my go-to printer for all my print work. They always do an exceptional job on whatever I give them and always go the extra mile. Plus, they are very knowledgeable and personable so its a pleasure working with them!</p>
          &mdash; Scott P. Nov 4, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>High Mountain Graphics has provided us with beautiful stationary and business cards for years. A pleasure to work with, and we're pleased that they are committed to using recycled materials.</p>
          &mdash; Kara Davis Nov 4, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>These guys are great. You deal with the owner and he really cares about delivering a great product. I talked to a lot of people before finally connecting with Rick and he was by far the most responsive and easy to deal with. He will also go above and beyond to try and work with you on solutions for your brand and your budget!</p>
          &mdash; Debra L. Nov 6, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick and Terri are a pleasure to work with. All print jobs have been delivered in a timely fashion and with great attention to detail. From one small business to another, thank you!</p>
          &mdash; Chris M. Nov 12, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>High Mountain Graphics is awesome! I went to Rick at High Mountain Graphics to get my business cards done when I first started my business. I will definitely be going back in the near future.</p>
          &mdash; Keith M. Dec 31, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick Shields, President of High Mountain Graphics, has partnered with my firm, Eco Friendly Printer/Greg Barber Company, for over 10 years. Our clients love our service, and our environmental expertise. They also love how fast we move. Rick has a color background and can correct most files, which is crucial when we are doing rush work. He can also create new artwork. So, Rick can handle our work from concept to completion. I made the correct choice in aligning myself with High Mountain Graphics.</p>
          &mdash; Greg Barber Jan 13, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>Fantastic service.  Very well priced and Rick help me modify a draft that made it look better and more readable.</p>
          &mdash; Rich M. Jan 28, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>Great company to work with. Always a pleasure doing business with High Mountain Graphics. Saved us many of times with fast shipping and great customer service. Keep up the good work.</p>
          &mdash; John A. April 19, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>At the office, we are super happy with the service they provide. Staff is super friendly, very responsive and they really know what they are doing. We were impressed with the quality of the jobs that we received. Great company to work with, excellent quality with fair price.</p>
          &mdash; Priscila F. June 7, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>We love working with Rick.  He's always reliable, quality of work is above standard and price is reasonable.  Outstanding customer service and highly recommend him for your business needs.</p>
          &mdash; Arti P. July 18, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>Great experience working with High Mountain Graphics. Rick was responsive and most accommodating with pricing. We received our brochures so fast and everything looked great!  Highly recommend.</p>
          &mdash; Th M. Aug 24, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick and the team at High Mountain Graphics are fantastic - high quality work and excellent customer service. I've worked with Rick for years and always keep the relationship going wherever I work - he makes the whole process so painless. I also love the commitment to the environment.</p>
          &mdash; Halle B. Aug 24, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick was very friendly and did a great job! He worked with me to get the print that I wanted in a timely, affordable manner. I had a great experience working with High Mountain Graphics.</p>
          &mdash; Tim B. Sept 14, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>High Mountain Graphics is so good! We had such a lovely experience with this company. Rick is super nice, prompt, and helpful! The business cards they made are choice. Plus, we were able to really communicate with Rick as we worked to pick out the perfect business card for Witty Serendipity. We loved working with him as well as the work that was done! We love High Mountain Graphics and suggest you go to them if you want quality printing done!</p>
          &mdash; Witty Serendipity Oct 4, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>I can't say enough good things about High Mountain Graphics! I was in total panic mode when my marketing project deadline was approaching and all of the other avenues I tried for printing did not work out. My local guy that I regularly used was unresponsive, the online printing website would not accept my files, and I was out of options and feeling desperate. In came Rick and his team from High Mountain Graphics and saved the day. Rick went out of this was to ensure that everything was taken care of, calmed my nerves, and even came in far under budget! The team at High Mountain Graphics are as professional as they come. They worked quickly to solve all of my problems and I had my brochures in hand 2 days before the deadline. The quality of work is excellent, the customer service is beyond superior, and the prices are very reasonable. You simply cannot go wrong with Rick and High Mountain Graphics - I recommend them for any and all of your printing or marketing needs.</p>
          &mdash; Shannon H. Oct 18, 2016
        </blockquote>
        <hr>
        <blockquote>
          <p>Great products, awesome service, reasonable prices and perhaps the most environmentally sound printing company that you can find. Very happy with the experience! You can't go wrong with this business!</p>
          &mdash; Alberto G. Jan 11, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>I love working with Rick. Getting all my printing done is always easy, quick, and at a reasonable price. The quality is fantastic and Rick is quick to respond whenever I have questions. I know I can rely on High Mountain Graphics for a quick turnaround and fantastic customer service. Highly recommend!</p>
          &mdash; Karolina P. Jan 17, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>I am going to keep it very simple; if you need quality work done, go see Rick at High Mountain Graphics. As a designer, color and color theory is extremely important. High Mountain Graphics understand the importance of color. They don't just understand color, they understand every single detail that goes into the print process. I have worked with High Mountain for several projects, and they have never missed a deadline! If you want a printer that does great high quality work and is reliable, call High Mountain Graphics. Plain and simple, they get it done! I would give 6 stars if I could.</p>
          &mdash; Drew O. Jan 31, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>I've worked with High Mountain Graphics as both a company and an individual now. They have such a deep knowledge of professional quality printing they can maintain the highest standards, yet are able to suggest a combination of features tailored to the needs of every client, order and budget. All around, they offer exceptional customer service - the last of a dying breed. I will return to them every time for their beautiful and timely products (be it posters, brochures, booklets, business cards, etc.) and their helpful service.</p>
          &mdash; Paige Irvine Feb 5, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>Kudos to Rick and his High Mountain Graphics team for creating my company's terrific new website. It's exactly what I wanted. It has been a pleasure working with Rick on this project. Then again, I've hired High Mountain Graphics for other projects in years past, so I knew exactly what to expect!</p>
          &mdash; Albert Stampone May 11, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>Excellent service with quick turnaround. Print quality is unparallelled. Eco-friendly with use of sustainably sourced paper and non-toxic, organic toner.</p>
          &mdash; Bill Ruddick May 2, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>I can not thank Rick and High Mountain Graphics enough for the amazing invitations which I needed printed. With speedy customer service I am sure to come back with all my future jobs! My client is so happy with her invitations and the speediness of the job. Rick and the team made it possible to work with my clients budget and still produce amazing quality! Thank you so much and I'll be back again for sure!!!</p>
          &mdash; Mary Skretkowicz May 23, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>The owners of High Mountain Graphics, Rick and Terri, are amazing to work with! I had last minute invitations that needed to be copied and printed, and they were super helpful and made it happen! Not only were they fast and reliable, but they are super friendly to work with.</p>
          &mdash; Mary R. May 31, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>High Mountain Graphics has exceeded all expectation for our printing needs! We searched for a very long time and went through many printing companies before landing on these amazing people. Rick and Terri are so friendly and have gone above and beyond with every project. As a company, there was still a lot to learn on our end about the 'printing/paper' world and Rick has been so patient and knowledgeable. They are a blessing to our company!! I HIGHLY recommend.</p>
          &mdash; Annmarie Gianni S. Aug 14, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>Great customer service, prices, and great work. Couldn't ask for anything better. I highly recommend this business!</p>
          &mdash; John H. Sept 28, 2017
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick was fantastic throughout the entire process and very responsive to all my e-mails. Quality of business cards was outstanding. Don't waste your time buying business cards online to save a few dollars. Support your local printer and contact High Mountain Graphics today!</p>
          &mdash; M.P. June 3, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick is the man…always awesome to work with these guys never a disappointment.</p>
          &mdash; A.J. April 15, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>I had a great experience with this company. They were fast, The quality was great. I felt very comfortable working with them. They have been in business a long time . It shows how they treat their customers. Thanks High Mountain!</p>
          &mdash; Doug Polifron April 15, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick, was fantastic throughout the entire process and very responsive to all my e-mails. Quality of business cards was outstanding. Don't waste your time buying business cards online to save a few dollars. Support your local printer and contact High Mountain Graphics today!</p>
          &mdash; M. Policastro June 3, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>I do a lot of printing in My business and have worked with many shops. High mountain graphics is by far the most reasonable priced and the most professional of them all. Owner Rick is patient and caring every step of the way. It is with great pleasure that I give High mountain this Review. I would recommend them as I have in the past to anyone that needs Fast Reliable printing at a fair price.</p>
          &mdash; George Sernack Dec 9, 2014
        </blockquote>
        <hr>
        <blockquote>
          <p>I have been using High Mountain Graphics for a few years for all of the printed materials for my small business, and HMG is exceptional. The work is impeccable, the price is always right, and Rick and team are a true pleasure to work with. I cannot recommend them enough!!</p>
          &mdash; Leonisa Ardizzone (<a target="_blank" href="http://www.storefrontscience.com/">Storefront Science</a>) March 2, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>I love working with Rick and his company High Mountain Graphics on all my printing projects. Rick knows how to do quality work for a fair price with a quick turnaround. I couldn't recommend his work highly enough.</p>
          &mdash; RJ Diaz (<a target="_blank" href="http://www.iciny.com">Integrity Contracting Inc.</a>) Feb 27, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>High Mountain Graphics was wonderful when we needed 50/50 tickets printed for a school function. Rick and his wife Terri are an absolute pleasure to work with. Not only did we receive the tickets in a timely manner but at a very reasonable price as well. Highly recommend them.</p>
          &mdash; Aime Espie (<a target="_blank" href="http://www.nhpto.org/Nan/index.cfm">NHPTO</a>) Feb 25, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>I needed business cards on short notice for a professional conference. After reading some reviews I contacted Rick-- and I am so glad that I did! He designed a beautiful, professional-looking card for me that exceeded my expectations. He is easy to speak to, communicating clearly and efficiently. He worked well within my time constraints and delivered a high-quality product at a very reasonable price. I felt confident and proud handing the cards out to new contacts. I definitely recommend High Mountain Graphics!</p>
          &mdash; Katherine Webster Feb 22, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>Rick is the very best! I am always in impossible situations but High Mountain Graphics makes all things printable possible.  I adore them!</p>
          &mdash; Sharon Hackney-Robinson (<a target="_blank" href="http://www.meandthegirls.com/">Me &amp; The Girls</a>) Feb 25, 2015
        </blockquote>
        <hr>
        <blockquote>
          <p>Not only did High Mountain Graphics provide a good price for my printing needs, they even gave me design advice on the business cards I wanted to print. High Mountain Graphics really cares for customer's needs, and I appreciate that. I would definitely recommend this place to anyone who needs quality printing done.</p>
          &mdash; Anish Kshatriya Nov 18, 2014
        </blockquote>
        <hr>
        <blockquote>
          <p>I had a great experience having High Mountain Graphics print my business card. I just went to their website, uploaded the file, and within a couple of days, the cards arrived at my door. Such little effort on my part! The quality and service were excellent! Highly recommended.</p>
          &mdash; Jill Baez Feb 13, 2012
        </blockquote>
        <hr>
        <blockquote>
          <p>We love working with High Mountain Graphics. Outstanding customer service, above average quality of work and reasonably priced. We highly recommend HMG for your business needs.</p>
          &mdash; Amanda Davis Jul 18, 2016
        </blockquote>
      </div>
    </section>
    <footer>
        <?php echo $copyright; ?>
    </footer>
  </div>
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script>
    window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')
  </script>
  <?php echo $scripts; ?>
</body>

</html>
