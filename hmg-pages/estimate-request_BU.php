<?
if(isset($_REQUEST["uiSubmit"]))
{
	$message = "Following Query has been submitted: <br />";
	$message .= "-------------------------------------------<br />";
	$message .= "<strong>CONTACT INFORMATION</strong><br />";	
	$message .= "<strong>Title: </strong> " . $_REQUEST["uiTitle"] . "<br />";
	$message .= "<strong>Name: </strong> " . $_REQUEST["uiName"] . "<br />";
	$message .= "<strong>Company: </strong> " . $_REQUEST["uiCompany"] . "<br />";	
	$message .= "<strong>Address: </strong> " . $_REQUEST["uiAddress"] . "<br />";
	$message .= "<strong>City: </strong> " . $_REQUEST["uiCity"] . "<br />";
	$message .= "<strong>Country: </strong> " . $_REQUEST["uiCountry"] . "<br />";
	$message .= "<strong>Phone Number: </strong> " . $_REQUEST["uiPhone"] . "<br />";
	$message .= "<strong>Fax Number: </strong> " . $_REQUEST["uiFax"] . "<br />";
	$message .= "<strong>Cell Number: </strong> " . $_REQUEST["uiCell"] . "<br />";
	$message .= "<strong>Email: </strong> " . $_REQUEST["uiEmail"] . "<br />";
	$message .= "<strong>Perferred Response: </strong> " . $_REQUEST["uiResponse"] . "<br />";
	$message .= "<strong>Artwork Provided:: </strong> " . $_REQUEST["uiArtwork"] . "<br /><br />";
	
	$message .= "<strong>GENERAL PROJECT INFORMATION</strong><br />";	
	$message .= "<strong>PO Number: </strong> " . $_REQUEST["uiPONumber"] . "<br />";
	$message .= "<strong>Project Name: </strong> " . $_REQUEST["uiProjectName"] . "<br />";
	$message .= "<strong>Project Due Date: </strong> " . $_REQUEST["uiProjectDate"] . "<br />";	
	$message .= "<strong>Reprint: </strong> " . $_REQUEST["uiReprint"] . "<br />";
	$message .= "<strong>Proof Type: </strong> " . $_REQUEST["uiProofType"] . "<br /><br />";
	
	
	$message .= "<strong>JOB INFORMATION</strong><br />";
	$message .= "<strong>Description: </strong> " . $_REQUEST["uiJobDescription"] . "<br />";
	$message .= "<strong>Quantity: </strong> " . $_REQUEST["uiJobQty"] . "<br />";
	$message .= "<strong>Size Flat: </strong> " . $_REQUEST["uiJobSizeFlat"] . "<br />";	
	$message .= "<strong>Size Folded: </strong> " . $_REQUEST["uiJobSizeFolded"] . "<br />";
	$message .= "<strong>Side(s): </strong> " . $_REQUEST["uiJobSides"] . "<br />";
	$message .= "<strong>Ink Color(s): </strong> " . $_REQUEST["uiJobColors"] . "<br />";
	$message .= "<strong>Stock: </strong> " . $_REQUEST["uiJobStock"] . "<br />";
	$message .= "<strong>Coating: </strong> " . $_REQUEST["uiJobCoating"] . "<br />";
	
	$message .= "<strong>Bindery Specifications: </strong><br /><ul>";	
	if(isset($_REQUEST["uiSpecifications1"]))
	{
		$message .= "<li>Collating</li>";
	}
	if(isset($_REQUEST["uiSpecifications2"]))
	{
		$message .= "<li>Folding</li>";
	}
	if(isset($_REQUEST["uiSpecifications3"]))
	{
		$message .= "<li>Drilling</li>";
	}
	if(isset($_REQUEST["uiSpecifications4"]))
	{
		$message .= "<li>Numbering</li>";
	}
	if(isset($_REQUEST["uiSpecifications5"]))
	{
		$message .= "<li>Padding</li>";
	}
	if(isset($_REQUEST["uiSpecifications6"]))
	{
		$message .= "<li>Perforation</li>";
	}
	if(isset($_REQUEST["uiSpecifications7"]))
	{
		$message .= "<li>Paper Banding</li>";
	}
	if(isset($_REQUEST["uiSpecifications8"]))
	{
		$message .= "<li>Perfect Binding</li>";
	}
	if(isset($_REQUEST["uiSpecifications9"]))
	{
		$message .= "<li>Saddle Stitching</li>";
	}
	if(isset($_REQUEST["uiSpecifications10"]))
	{
		$message .= "<li>Stapling</li>";
	}
	if(isset($_REQUEST["uiSpecifications11"]))
	{
		$message .= "<li>Stringing</li>";
	}
	if(isset($_REQUEST["uiSpecifications12"]))
	{
		$message .= "<li>Tabbing</li>";
	}
	if(isset($_REQUEST["uiSpecifications13"]))
	{
		$message .= "<li>3-Hole Punching</li>";
	}
	if(isset($_REQUEST["uiSpecifications14"]))
	{
		$message .= "<li>Die Cutting</li>";
	}
	if(isset($_REQUEST["uiSpecifications15"]))
	{
		$message .= "<li>Embossing</li>";
	}
	if(isset($_REQUEST["uiSpecifications16"]))
	{
		$message .= "<li>Foil Stamping</li>";
	}
	if(isset($_REQUEST["uiSpecifications17"]))
	{
		$message .= "<li>Laminating</li>";
	}
	if(isset($_REQUEST["uiSpecifications18"]))
	{
		$message .= "<li>GBC Binding</li>";
	}
	if(isset($_REQUEST["uiSpecifications19"]))
	{
		$message .= "<li>Wire-O Binding</li>";
	}							
	$message .= "</ul>";			
	
	$message .= "<strong>Additional Bindery Specifications: </strong> " . $_REQUEST["uiJobAddSpecifications"] . "<br />";
	$message .= "<strong>Comments/Instructions:: </strong> " . $_REQUEST["uiJobComments"] . "<br />";
	
	//////////////// Saving File /////////////////
	if($_FILES['uiRelatedFile'])
	{
// Where the file is going to be placed

	function GetLastIndex($str)
	{
		$val=0;
		for($i=0;$i<strlen($str);$i++)
		{
			if($str[$i] == '/')
			{
				$val=$i;
			}
		}
		return $val;
	}
	$filepath= "http://" .$_SERVER['HTTP_HOST'] . substr($_SERVER['PHP_SELF'],0,GetLastIndex($_SERVER['PHP_SELF'])); 
	
$target_path = "uploads/";

/* Add the original filename to our target path. 
Result is "uploads/filename.extension" */
$target_path = $target_path . basename( $_FILES['uiRelatedFile']['name']);
	$filepath .= "/" . $target_path;
copy($_FILES['uiRelatedFile']['tmp_name'],$target_path); 
	}
	//////////////// End Saving File ////////////////
	
		$message .= "<strong>Attachments: </strong> <a href='" . $filepath . "'>" . $filepath . "</a><br />";	
		

/// Additonal Jobs
for($m=1;$m<$_REQUEST["uiTotalJobs"];$m++)
{
	$message .= "<strong>JOB INFORMATION (" . ($m+1) . ")</strong><br />";
	$message .= "<strong>Description: </strong> " . $_REQUEST["uiJobDescription".$m] . "<br />";
	$message .= "<strong>Quantity: </strong> " . $_REQUEST["uiJobQty".$m] . "<br />";
	$message .= "<strong>Size Flat: </strong> " . $_REQUEST["uiJobSizeFlat".$m] . "<br />";	
	$message .= "<strong>Size Folded: </strong> " . $_REQUEST["uiJobSizeFolded".$m] . "<br />";
	$message .= "<strong>Side(s): </strong> " . $_REQUEST["uiJobSides".$m] . "<br />";
	$message .= "<strong>Ink Color(s): </strong> " . $_REQUEST["uiJobColors".$m] . "<br />";
	$message .= "<strong>Stock: </strong> " . $_REQUEST["uiJobStock".$m] . "<br />";
	$message .= "<strong>Coating: </strong> " . $_REQUEST["uiJobCoating".$m] . "<br />";
	
	$message .= "<strong>Bindery Specifications: </strong><br /><ul>";	
	if(isset($_REQUEST["uiSpecifications1".$m]))
	{
		$message .= "<li>Collating</li>";
	}
	if(isset($_REQUEST["uiSpecifications2".$m]))
	{
		$message .= "<li>Folding</li>";
	}
	if(isset($_REQUEST["uiSpecifications3".$m]))
	{
		$message .= "<li>Drilling</li>";
	}
	if(isset($_REQUEST["uiSpecifications4".$m]))
	{
		$message .= "<li>Numbering</li>";
	}
	if(isset($_REQUEST["uiSpecifications5".$m]))
	{
		$message .= "<li>Padding</li>";
	}
	if(isset($_REQUEST["uiSpecifications6".$m]))
	{
		$message .= "<li>Perforation</li>";
	}
	if(isset($_REQUEST["uiSpecifications7".$m]))
	{
		$message .= "<li>Paper Banding</li>";
	}
	if(isset($_REQUEST["uiSpecifications8".$m]))
	{
		$message .= "<li>Perfect Binding</li>";
	}
	if(isset($_REQUEST["uiSpecifications9".$m]))
	{
		$message .= "<li>Saddle Stitching</li>";
	}
	if(isset($_REQUEST["uiSpecifications10".$m]))
	{
		$message .= "<li>Stapling</li>";
	}
	if(isset($_REQUEST["uiSpecifications11".$m]))
	{
		$message .= "<li>Stringing</li>";
	}
	if(isset($_REQUEST["uiSpecifications12".$m]))
	{
		$message .= "<li>Tabbing</li>";
	}
	if(isset($_REQUEST["uiSpecifications13".$m]))
	{
		$message .= "<li>3-Hole Punching</li>";
	}
	if(isset($_REQUEST["uiSpecifications14".$m]))
	{
		$message .= "<li>Die Cutting</li>";
	}
	if(isset($_REQUEST["uiSpecifications15".$m]))
	{
		$message .= "<li>Embossing</li>";
	}
	if(isset($_REQUEST["uiSpecifications16".$m]))
	{
		$message .= "<li>Foil Stamping</li>";
	}
	if(isset($_REQUEST["uiSpecifications17".$m]))
	{
		$message .= "<li>Laminating</li>";
	}
	if(isset($_REQUEST["uiSpecifications18".$m]))
	{
		$message .= "<li>GBC Binding</li>";
	}
	if(isset($_REQUEST["uiSpecifications19".$m]))
	{
		$message .= "<li>Wire-O Binding</li>";
	}							
	$message .= "</ul>";			
	
	$message .= "<strong>Additional Bindery Specifications: </strong> " . $_REQUEST["uiJobAddSpecifications".$m] . "<br />";
	$message .= "<strong>Comments/Instructions:: </strong> " . $_REQUEST["uiJobComments".$m] . "<br />";
	
	//////////////// Saving File /////////////////
	if($_FILES['uiRelatedFile'.$m])
	{
// Where the file is going to be placed
	$filepath= "http://" .$_SERVER['HTTP_HOST'] . substr($_SERVER['PHP_SELF'],0,GetLastIndex($_SERVER['PHP_SELF'])); 
	
$target_path = "uploads/";

/* Add the original filename to our target path. 
Result is "uploads/filename.extension" */
$target_path = $target_path . basename( $_FILES['uiRelatedFile'.$m]['name']);
	$filepath .= "/" . $target_path;
copy($_FILES['uiRelatedFile'.$m]['tmp_name'],$target_path); 
	}
	//////////////// End Saving File ////////////////
	
		$message .= "<strong>Attachments: </strong> <a href='" . $filepath . "'>" . $filepath . "</a><br />";
}
//// End Aditional Jobs		

		
	$message .= "-------------------------------------------<br />";
	
		$header="Content-Type: text/html\r\nFrom:info@highmountaingraphics.com";
		mail("rick@highmountaingraphics.com,info@highmountaingraphics.com","Estimate Request",$message,$header);
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>

	<head>
		<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">
		<meta name="generator" content="Adobe GoLive">
		<title>HMG - Request an Estimate</title>
		<link href="../hmg-css/hmg01.css" rel="stylesheet" type="text/css" media="all">
		<csscriptdict import>
			<script type="text/javascript" src="../GeneratedItems/CSScriptLib.js"></script>
		</csscriptdict>
		<csactiondict>
			<script type="text/javascript"><!--
			var nextrow=1;			
var preloadFlag = false;
function preloadImages() {
	if (document.images) {
		pre_button01_over = newImage('../hmg-images/menu/button01-over.jpg');
		pre_button02_over = newImage('../hmg-images/menu/button02-over.jpg');
		pre_button03_over = newImage('../hmg-images/menu/button03-over.jpg');
		pre_button04_over = newImage('../hmg-images/menu/button04-over.jpg');
		pre_button05_over = newImage('../hmg-images/menu/button05-over.jpg');
		preloadFlag = true;
	}
}

function ShowIT()
{
	if(nextrow<=4)
	{
		document.getElementById("uiJobInfo" + nextrow).style.display="block";
		nextrow++;
//		uiTotalJobs
		document.getElementById("uiTotalJobs").value=nextrow;
	}
	else
	{
//		uiAddJobInfo
		document.getElementById("uiAddJobInfo").style.display="none";
	}
}

// --></script>
		</csactiondict>
        <style type="text/css">
<!--
.style1 {color: #FFFFFF}
-->
        </style>
</head>

	<body class="background01" onLoad="preloadImages();" >
		<div align="center">
			<table width="1000" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="1024"><img src="../hmg-images/components/logo05.jpg" alt="" height="120" width="240" border="0">&nbsp;&nbsp;&nbsp;<a onMouseOver="changeImages('button01','../hmg-images/menu/button01-over.jpg');return true" onMouseOut="changeImages('button01','../hmg-images/menu/button01.jpg');return true" href="../index.html"><img id="button01" src="../hmg-images/menu/button01.jpg" alt="" name="button01" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onMouseOver="changeImages('button02','../hmg-images/menu/button02-over.jpg');return true" onMouseOut="changeImages('button02','../hmg-images/menu/button02.jpg');return true" href="design.html"><img id="button02" src="../hmg-images/menu/button02.jpg" alt="" name="button02" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onMouseOver="changeImages('button03','../hmg-images/menu/button03-over.jpg');return true" onMouseOut="changeImages('button03','../hmg-images/menu/button03.jpg');return true" href="portfolio.html"><img id="button03" src="../hmg-images/menu/button03.jpg" alt="" name="button03" height="35" width="110" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onMouseOver="changeImages('button04','../hmg-images/menu/button04-over.jpg');return true" onMouseOut="changeImages('button04','../hmg-images/menu/button04.jpg');return true" href="prepress.html"><img id="button04" src="../hmg-images/menu/button04.jpg" alt="" name="button04" height="35" width="182" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a onMouseOver="changeImages('button05','../hmg-images/menu/button05-over.jpg');return true" onMouseOut="changeImages('button05','../hmg-images/menu/button05-over.jpg');return true" href="contactus.html"><img id="button05" src="../hmg-images/menu/button05-over.jpg" alt="" name="button05" border="0"></a></td>
				</tr>
				<tr>
					<td class="footer" valign="middle" width="1024"><img src="../hmg-images/components/Line.jpg" alt="" height="4" width="1024" border="0"><br>
					</td>
				</tr>
				<tr height="30">
					<td valign="middle" width="1024" height="30" background="../hmg-images/menu/bar05.jpg">
						<div align="center">
							<span class="menulinks"><a class="menulinks" href="contactus.html">Contact Us</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="menulinks" href="estimate-request.php">Request&nbsp; an Estimate&nbsp;</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="menulinks" href="online-order.php">Place an Order</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></div>
					</td>
				</tr>
				<tr height="500">
					<td valign="top" width="1024" height="500" background="../hmg-images/backgrounds/background05.jpg">
						<div align="center">
							<table width="850" border="0" cellspacing="0" cellpadding="0">
								<tr height="80">
									<td class="body" valign="top" height="80"></td>
								</tr>
								<tr>
									<td class="body" valign="top">
										<div id="scroll3" class="scrollable" style=" width:790;height:325;overflow:auto; ">
										<?php
                                        	if(!isset($_REQUEST["uiSubmit"]))
											{
										?>
											<form class="body-form" enctype="multipart/form-data" action="" method="POST">
												<table border="0" cellspacing="0" cellpadding="0">
													<tr>
														<td>
															<table class="body-form" width="100%" border="0" align="center" cellpadding="2" cellspacing="2">
																<tr>
																	<td class="form" valign="top" width="200"><span class="subtitle"><b><font color="#8c98a4">CONTACT INFORMATION </font></b></span></td>
																	<td class="form" valign="top" width="10"></td>
																	<td valign="top" width="200" class="form"></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b>Title:</b><br>
																		<select name="uiTitle" id="uiTitle">
																			<option value="N/A">N/A</option>
																			<option value="Mr.">Mr.</option>
																			<option value="Mrs.">Mrs.</option>
																			<option value="Ms.">Ms.</option>
																			<option value="Miss.">Miss.</option>
																			<option value="Dr.">Dr.</option>
																			<option value="Fr.">Fr.</option>
																		</select></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200"><b>Name:</b><br>
																		<input type="text" _4_name="name" name="uiName" value="" size="40" id="uiName"></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b>Company:</b><br>
																		<input type="text" name="uiCompany" size="40" id="uiCompany"></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200"><b>Address:</b><br>
																		<input type="text" name="uiAddress" size="40" id="uiAddress"></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b>City, State/Province, Zip/Postal Code:<br>
																		</b><input type="text" name="uiCity" size="40" id="uiCity"></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200"><b>Country:<br>
																		</b><input type="text" name="uiCountry" size="40" id="uiCountry"></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b>Phone Number:</b><br>
																		<input type="text" name="uiPhone" size="40" id="uiPhone"></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200"><b>Fax Number:<br>
																		</b><input type="text" name="uiFax" size="40" id="uiFax"></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b>Cell Number:<br>
																		</b><input type="text" name="uiCell" size="40" id="uiCell"></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200" height="39"><b>E-mail Address:</b><br>
																		<input type="text" name="uiEmail" size="40" id="uiEmail"></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200" height="39"><b>Artwork Provided:</b><br>
																		<select name="uiArtwork" td class="form-misc" id="uiArtwork">
																			<option value="CD/DVD">CD/DVD</option>
																			<option value="E-mail">E-mail</option>
																			<option value="FTP">FTP</option>
																			<option value="Upload" selected>Upload</option>
																			<option value="Hard Copy Original(s)">Hard Copy Original(s)</option>
																		</select></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200" height="39"><b>Preferred Response:<br>
																		</b><span class="form"><select name="uiResponse" td class="form-misc" id="uiResponse">
																				<option value="Email" selected>Email</option>
																				<option value="Phone">Phone</option>
																				<option value="Fax">Fax</option>
																				<option value="Cell">Cell</option>
																			</select>
                            </span></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200">&nbsp;</td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200">&nbsp;</td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b><font color="#8c98a4"><br>
																				<span class="subtitle">GENERAL PROJECT INFORMATION</span></font></b></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200"></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b>PO Number:<br>
																		</b><input type="text" _4_name="name" name="uiPONumber" value="" size="40" id="uiPONumber"></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200"><b>Project Name:<br>
																		</b><input type="text" name="uiProjectName" size="40" id="uiProjectName"></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b>Project Due Date:<br>
																		</b><input type="text" _4_name="name" name="uiProjectDate" value="" size="40" id="uiProjectDate"></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200"><b>Reprint<br>
																		</b><select name="uiReprint" td class="form-misc" id="uiReprint">
																			<option value="Yes (Match Sample)">YES (Match Sample)</option>
																			<option value="No" selected>NO</option>
																		</select></td>
																</tr>
																<tr>
																	<td class="form" valign="top" width="200"><b>Proof Type:</b><br>
																		<select name="uiProofType" td class="form-misc" id="uiProofType">
																			<option value="PDF" selected>PDF (Soft Proof) For Content</option>
																			<option value="Digital">Digital (Laser Proof) Digital Color</option>
																			<option value="Offset">Offset (Press Proof) Color Critical</option>
																		</select></td>
																	<td class="form" valign="top" width="10"></td>
																	<td class="form" valign="top" width="200">&nbsp;</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="body-form">
																<tr>
																	<td class="form" valign="top"><b><font color="#8c98a4"><br>
																				<span class="subtitle">JOB INFORMATION</span></font></b></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Description:</b><br>
																		<input type="text" _4_name="name" name="uiJobDescription" value="" size="40" id="uiJobDescription"><br>
																		 (Example: Business Card, Letterhead, Envelope, etc.)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Quantity:</b><br>
																		<input type="text" name="uiJobQty" size="40" id="uiJobQty"><br>
																		(Example: 500, 1M, 2500, 5M)  M = Thousand</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Size Flat:</b><br>
																		<input type="text" _4_name="name" name="uiJobSizeFlat" value="" size="40" id="uiJobSizeFlat"><br>
																		(Example: 17&quot;w x 11&quot;h)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Size Folded:</b><br>
																		<input type="text" name="uiJobSizeFolded" size="40" id="uiJobSizeFolded"><br>
																		(Example: 8.5&quot;w x 11&quot;h)</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Side(s):</b><br>
																		<input type="text" _4_name="name" name="uiJobSides" value="" size="40" id="uiJobSides"><br>
																		 (Example: Single Sided, Double Sided)<br>
																	</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Ink Color(s):</b><br>
																		<input type="text" name="uiJobColors" size="40" id="uiJobColors"><br>
																		 (Example: Black, PMS, 4C Process)</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Stock:</b><br>
																		<input type="text" _4_name="name" name="uiJobStock" value="" size="40" id="uiJobStock"><br>
																		 (Example: 70 LB White Text, 100 LB Natural Cover)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Coating:</b><br>
																		<input type="text" name="uiJobCoating" size="40" id="uiJobCoating"><br>
																		 (Example: Matte/Gloss, Aqueous, Varnish, UV) Leave blank for uncoated</td>
																</tr>
																<tr>
																	<td class="form" colspan="3" valign="top"><b><strong>Bindery Specifications</strong><font color="#8c98a4"><br>
																			</font></b>
																		 This project will require the following:<br>
																		<br>
																		
																		 Please check all that apply</td>
																</tr>
																<tr>
																	<td class="body-form2" colspan="3" valign="top">
																		<div class="body-form2">
																			<table class="body-form2" width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications1" id="uiSpecifications1"> </label> Collating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications2" id="uiSpecifications2"> </label> Folding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications3" id="uiSpecifications3"> </label> Drilling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications4" id="uiSpecifications4"> </label> Numbering&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications5" id="uiSpecifications5"> </label> Padding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications6" id="uiSpecifications6"> </label> Perforation&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications7" id="uiSpecifications7"> </label> Paper Banding</td>
																					<td class="title"><input type="checkbox" name="uiSpecifications8" id="uiSpecifications8"> Perfect Binding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications9" id="uiSpecifications9"> </label> Saddle Stitching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications10" id="uiSpecifications10"> </label> Stapling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications11" id="uiSpecifications11"> </label> Stringing</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications12" id="uiSpecifications12"> </label> Tabbing&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications13" id="uiSpecifications13"> </label> 3-Hole Punching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications14" id="uiSpecifications14"> </label> Die Cutting&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications15" id="uiSpecifications15"> </label> Embossing&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications16" id="uiSpecifications16"> </label> Foil Stamping&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications17" id="uiSpecifications17"> </label> Laminating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications18" id="uiSpecifications18"> </label> GBC Binding&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications19" id="uiSpecifications19"> </label> Wire-O Binding</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Additional Bindery Specifications:<br>
																		</b><label><textarea name="uiJobAddSpecifications" id="uiJobAddSpecifications" cols="40" rows="5"></textarea>
                            </label></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Comments/Instructions:<br>
																		</b><label><textarea name="uiJobComments" id="uiJobComments" cols="40" rows="5"></textarea>
                            </label></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b><font color="#8c98a4"><br>
																				<span class="subtitle">FILE UPLOAD</span></font></b></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																</tr>
																<tr>
																	<td class="form" colspan="3" valign="top">Please upload your zipped, stuffed, or compressed file(s) by clicking &quot;choose file&quot; and then selecting your file.<br>
																		 Our system can accept transfers of up to 100MB per form submission. If your files exceed this limit, you may either<br>
																		 upload more files by submitting an additional form, send us your files on CD or DVD, or call for private ftp access.<br>Be sure to use the same &quot;project name&quot; and/or reference number that was used on the original submission form.</td>
																</tr>
																<tr height="39">
																	<td class="form" valign="top" height="39"><b>Choose file(s) to be uploaded:<br>
																			<label><input type="file" name="uiRelatedFile" id="uiRelatedFile">
                              </label></b></td>
																	<td class="form" valign="top" height="39"></td>
																	<td class="form" valign="top" height="39">&nbsp;</td>
																</tr>
															</table>
														<td>
													</tr>
													<tr id="uiJobInfo1" style="display:none;">
														<td>
															<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="body-form">
																<tr>
																	<td class="form" valign="top"><b><font color="#8c98a4"><br>
																				<span class="subtitle">JOB INFORMATION(2)</span></font></b></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Description:</b><br>
																		<input type="text" _4_name="name" name="uiJobDescription1" value="" size="40" id="uiJobDescription1"><br>
																		 (Example: Business Card, Letterhead, Envelope, etc.)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Quantity:</b><br>
																		<input type="text" name="uiJobQty1" size="40" id="uiJobQty1"><br>
																		(Example: 500, 1M, 2500, 5M)  M = Thousand</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Size Flat:</b><br>
																		<input type="text" _4_name="name" name="uiJobSizeFlat1" value="" size="40" id="uiJobSizeFlat1"><br>
																		(Example: 17&quot;w x 11&quot;h)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Size Folded:</b><br>
																		<input type="text" name="uiJobSizeFolded1" size="40" id="uiJobSizeFolded1"><br>
																		(Example: 8.5&quot;w x 11&quot;h)</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Side(s):</b><br>
																		<input type="text" _4_name="name" name="uiJobSides1" value="" size="40" id="uiJobSides1"><br>
																		 (Example: Single Sided, Double Sided)<br>
																	</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Ink Color(s):</b><br>
																		<input type="text" name="uiJobColors1" size="40" id="uiJobColors1"><br>
																		 (Example: Black, PMS, 4C Process)</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Stock:</b><br>
																		<input type="text" _4_name="name" name="uiJobStock1" value="" size="40" id="uiJobStock1"><br>
																		 (Example: 70 LB White Text, 100 LB Natural Cover)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Coating:</b><br>
																		<input type="text" name="uiJobCoating1" size="40" id="uiJobCoating1"><br>
																		 (Example: Matte/Gloss, Aqueous, Varnish, UV) Leave blank for uncoated</td>
																</tr>
																<tr>
																	<td class="form" colspan="3" valign="top"><b><strong>Bindery Specifications</strong><font color="#8c98a4"><br>
																			</font></b>
																		 This project will require the following:<br>
																		<br>
																		
																		 Please check all that apply</td>
																</tr>
																<tr>
																	<td class="body-form2" colspan="3" valign="top">
																		<div class="body-form2">
																			<table class="body-form2" width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications11" id="uiSpecifications11"> </label> Collating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications21" id="uiSpecifications21"> </label> Folding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications31" id="uiSpecifications31"> </label> Drilling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications41" id="uiSpecifications41"> </label> Numbering&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications51" id="uiSpecifications51"> </label> Padding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications61" id="uiSpecifications61"> </label> Perforation&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications71" id="uiSpecifications71"> </label> Paper Banding</td>
																					<td class="title"><input type="checkbox" name="uiSpecifications81" id="uiSpecifications81"> Perfect Binding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications91" id="uiSpecifications91"> </label> Saddle Stitching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications101" id="uiSpecifications101"> </label> Stapling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications111" id="uiSpecifications111"> </label> Stringing</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications121" id="uiSpecifications121"> </label> Tabbing&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications131" id="uiSpecifications131"> </label> 3-Hole Punching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications141" id="uiSpecifications14"> </label> Die Cutting&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications151" id="uiSpecifications151"> </label> Embossing&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications161" id="uiSpecifications161"> </label> Foil Stamping&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications171" id="uiSpecifications171"> </label> Laminating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications181" id="uiSpecifications181"> </label> GBC Binding&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications191" id="uiSpecifications191"> </label> Wire-O Binding</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Additional Bindery Specifications:<br>
																		</b><label><textarea name="uiJobAddSpecifications1" id="uiJobAddSpecifications1" cols="40" rows="5"></textarea>
                            </label></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Comments/Instructions:<br>
																		</b><label><textarea name="uiJobComments1" id="uiJobComments1" cols="40" rows="5"></textarea>
                            </label></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b><font color="#8c98a4"><br>
																				<span class="subtitle">FILE UPLOAD</span></font></b></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																</tr>
																<tr>
																	<td class="form" colspan="3" valign="top">Please upload your zipped, stuffed, or compressed file(s) by clicking &quot;choose file&quot; and then selecting your file.<br>
																		 Our system can accept transfers of up to 100MB per form submission. If your files exceed this limit, you may either<br>
																		 upload more files by submitting an additional form, send us your files on CD or DVD, or call for private ftp access.<br>Be sure to use the same &quot;project name&quot; and/or reference number that was used on the original submission form.</td>
																</tr>
																<tr height="39">
																	<td class="form" valign="top" height="39"><b>Choose file(s) to be uploaded:<br>
																			<label><input type="file" name="uiRelatedFile1" id="uiRelatedFile1">
                              </label></b></td>
																	<td class="form" valign="top" height="39"></td>
																	<td class="form" valign="top" height="39">&nbsp;</td>
																</tr>
															</table>
														<td>
													</tr>
													<tr id="uiJobInfo2" style="display:none;">
														<td>
															<table width="100%" border="0" align="center" cellpadding="2" cellspacing="2" class="body-form">
																<tr>
																	<td class="form" valign="top"><br>
																		<span class="subtitle"><b><font color="#8c98a4">JOB INFORMATION(3)</font></b></span></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Description:</b><br>
																		<input type="text" _4_name="name" name="uiJobDescription2" value="" size="40" id="uiJobDescription2"><br>
																		 (Example: Business Card, Letterhead, Envelope, etc.)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Quantity:</b><br>
																		<input type="text" name="uiJobQty2" size="40" id="uiJobQty2"><br>
																		(Example: 500, 1M, 2500, 5M)  M = Thousand</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Size Flat:</b><br>
																		<input type="text" _4_name="name" name="uiJobSizeFlat2" value="" size="40" id="uiJobSizeFlat2"><br>
																		(Example: 17&quot;w x 11&quot;h)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Size Folded:</b><br>
																		<input type="text" name="uiJobSizeFolded2" size="40" id="uiJobSizeFolded2"><br>
																		(Example: 8.5&quot;w x 11&quot;h)</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Side(s):</b><br>
																		<input type="text" _4_name="name" name="uiJobSides2" value="" size="40" id="uiJobSides2"><br>
																		 (Example: Single Sided, Double Sided)<br>
																	</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Ink Color(s):</b><br>
																		<input type="text" name="uiJobColors2" size="40" id="uiJobColors2"><br>
																		 (Example: Black, PMS, 4C Process)</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Stock:</b><br>
																		<input type="text" _4_name="name" name="uiJobStock2" value="" size="40" id="uiJobStock2"><br>
																		 (Example: 70 LB White Text, 100 LB Natural Cover)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Coating:</b><br>
																		<input type="text" name="uiJobCoating2" size="40" id="uiJobCoating2"><br>
																		 (Example: Matte/Gloss, Aqueous, Varnish, UV) Leave blank for uncoated</td>
																</tr>
																<tr>
																	<td class="form" colspan="3" valign="top"><b><strong>Bindery Specifications</strong><font color="#8c98a4"><br>
																			</font></b>
																		 This project will require the following:<br>
																		<br>
																		
																		 Please check all that apply</td>
																</tr>
																<tr>
																	<td class="body-form2" colspan="3" valign="top">
																		<div class="body-form2">
																			<table class="body-form2" width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications12" id="uiSpecifications12"> </label> Collating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications22" id="uiSpecifications22"> </label> Folding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications32" id="uiSpecifications32"> </label> Drilling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications42" id="uiSpecifications42"> </label> Numbering&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications52" id="uiSpecifications52"> </label> Padding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications62" id="uiSpecifications62"> </label> Perforation&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications72" id="uiSpecifications72"> </label> Paper Banding</td>
																					<td class="title"><input type="checkbox" name="uiSpecifications82" id="uiSpecifications82"> Perfect Binding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications92" id="uiSpecifications92"> </label> Saddle Stitching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications102" id="uiSpecifications102"> </label> Stapling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications112" id="uiSpecifications112"> </label> Stringing</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications122" id="uiSpecifications122"> </label> Tabbing&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications132" id="uiSpecifications132"> </label> 3-Hole Punching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications142" id="uiSpecifications14"> </label> Die Cutting&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications152" id="uiSpecifications152"> </label> Embossing&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications162" id="uiSpecifications162"> </label> Foil Stamping&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications172" id="uiSpecifications172"> </label> Laminating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications182" id="uiSpecifications182"> </label> GBC Binding&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications192" id="uiSpecifications192"> </label> Wire-O Binding</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Additional Bindery Specifications:<br>
																		</b><label><textarea name="uiJobAddSpecifications2" id="uiJobAddSpecifications2" cols="40" rows="5"></textarea>
                            </label></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Comments/Instructions:<br>
																		</b><label><textarea name="uiJobComments2" id="uiJobComments2" cols="40" rows="5"></textarea>
                            </label></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b><font color="#8c98a4"><br>
																				<span class="subtitle">FILE UPLOAD</span></font></b></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																</tr>
																<tr>
																	<td class="form" colspan="3" valign="top">Please upload your zipped, stuffed, or compressed file(s) by clicking &quot;choose file&quot; and then selecting your file.<br>
																		 Our system can accept transfers of up to 100MB per form submission. If your files exceed this limit, you may either<br>
																		 upload more files by submitting an additional form, send us your files on CD or DVD, or call for private ftp access.<br>Be sure to use the same &quot;project name&quot; and/or reference number that was used on the original submission form.</td>
																</tr>
																<tr height="39">
																	<td class="form" valign="top" height="39"><b>Choose file(s) to be uploaded:<br>
																			<label><input type="file" name="uiRelatedFile2" id="uiRelatedFile2">
                              </label></b></td>
																	<td class="form" valign="top" height="39"></td>
																	<td class="form" valign="top" height="39">&nbsp;</td>
																</tr>
															</table>
														<td>
													</tr>
													<tr id="uiJobInfo3" style="display:none;">
														<td>
															<table width="100%" border="0" align="center" cellpadding="3" cellspacing="3" class="body-form">
																<tr>
																	<td class="form" valign="top"><br>
																		<span class="subtitle"><font color="#8c98a4">JOB INFORMATION(4)</font></span></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Description:</b><br>
																		<input type="text" _4_name="name" name="uiJobDescription3" value="" size="40" id="uiJobDescription3"><br>
																		 (Example: Business Card, Letterhead, Envelope, etc.)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Quantity:</b><br>
																		<input type="text" name="uiJobQty3" size="40" id="uiJobQty3"><br>
																		(Example: 500, 1M, 2500, 5M)  M = Thousand</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Size Flat:</b><br>
																		<input type="text" _4_name="name" name="uiJobSizeFlat3" value="" size="40" id="uiJobSizeFlat3"><br>
																		(Example: 17&quot;w x 11&quot;h)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Size Folded:</b><br>
																		<input type="text" name="uiJobSizeFolded3" size="40" id="uiJobSizeFolded3"><br>
																		(Example: 8.5&quot;w x 11&quot;h)</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Side(s):</b><br>
																		<input type="text" _4_name="name" name="uiJobSides3" value="" size="40" id="uiJobSides3"><br>
																		 (Example: Single Sided, Double Sided)<br>
																	</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Ink Color(s):</b><br>
																		<input type="text" name="uiJobColors3" size="40" id="uiJobColors3"><br>
																		 (Example: Black, PMS, 4C Process)</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Stock:</b><br>
																		<input type="text" _4_name="name" name="uiJobStock3" value="" size="40" id="uiJobStock3"><br>
																		 (Example: 70 LB White Text, 100 LB Natural Cover)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Coating:</b><br>
																		<input type="text" name="uiJobCoating3" size="40" id="uiJobCoating3"><br>
																		 (Example: Matte/Gloss, Aqueous, Varnish, UV) Leave blank for uncoated</td>
																</tr>
																<tr>
																	<td class="form" colspan="3" valign="top"><b><strong>Bindery Specifications</strong><font color="#8c98a4"><br>
																			</font></b>
																		 This project will require the following:<br>
																		<br>
																		
																		 Please check all that apply</td>
																</tr>
																<tr>
																	<td class="body-form2" colspan="3" valign="top">
																		<div class="body-form2">
																			<table class="body-form2" width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications13" id="uiSpecifications13"> </label> Collating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications23" id="uiSpecifications23"> </label> Folding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications33" id="uiSpecifications33"> </label> Drilling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications43" id="uiSpecifications43"> </label> Numbering&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications53" id="uiSpecifications53"> </label> Padding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications63" id="uiSpecifications63"> </label> Perforation&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications73" id="uiSpecifications73"> </label> Paper Banding</td>
																					<td class="title"><input type="checkbox" name="uiSpecifications83" id="uiSpecifications83"> Perfect Binding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications93" id="uiSpecifications93"> </label> Saddle Stitching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications103" id="uiSpecifications103"> </label> Stapling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications113" id="uiSpecifications113"> </label> Stringing</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications123" id="uiSpecifications123"> </label> Tabbing&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications133" id="uiSpecifications133"> </label> 3-Hole Punching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications143" id="uiSpecifications14"> </label> Die Cutting&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications153" id="uiSpecifications153"> </label> Embossing&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications163" id="uiSpecifications163"> </label> Foil Stamping&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications173" id="uiSpecifications173"> </label> Laminating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications183" id="uiSpecifications183"> </label> GBC Binding&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications193" id="uiSpecifications193"> </label> Wire-O Binding</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Additional Bindery Specifications:<br>
																		</b><label><textarea name="uiJobAddSpecifications3" id="uiJobAddSpecifications3" cols="40" rows="5"></textarea>
                            </label></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Comments/Instructions:<br>
																		</b><label><textarea name="uiJobComments3" id="uiJobComments3" cols="40" rows="5"></textarea>
                            </label></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b><font color="#8c98a4"><br>
																				<span class="subtitle">FILE UPLOAD</span></font></b></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																</tr>
																<tr>
																	<td class="form" colspan="3" valign="top">Please upload your zipped, stuffed, or compressed file(s) by clicking &quot;choose file&quot; and then selecting your file.<br>
																		 Our system can accept transfers of up to 100MB per form submission. If your files exceed this limit, you may either<br>
																		 upload more files by submitting an additional form, send us your files on CD or DVD, or call for private ftp access.<br>Be sure to use the same &quot;project name&quot; and/or reference number that was used on the original submission form.</td>
																</tr>
																<tr height="39">
																	<td class="form" valign="top" height="39"><b>Choose file(s) to be uploaded:<br>
																			<label><input type="file" name="uiRelatedFile3" id="uiRelatedFile3">
                              </label></b></td>
																	<td class="form" valign="top" height="39"></td>
																	<td class="form" valign="top" height="39">&nbsp;</td>
																</tr>
															</table>
														<td>
													</tr>
													<tr id="uiJobInfo4" style="display:none;">
														<td>
															<table width="100%" border="0" align="center" cellpadding="4" cellspacing="4" class="body-form">
																<tr>
																	<td class="form" valign="top"><b><font color="#8c98a4"><br>
																				<span class="subtitle">JOB INFORMATION(5)</span></font></b></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																	<td valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Description:</b><br>
																		<input type="text" _4_name="name" name="uiJobDescription4" value="" size="40" id="uiJobDescription4"><br>
																		 (Example: Business Card, Letterhead, Envelope, etc.)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Quantity:</b><br>
																		<input type="text" name="uiJobQty4" size="40" id="uiJobQty4"><br>
																		(Example: 500, 1M, 2500, 5M)  M = Thousand</td>
																	<td valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Size Flat:</b><br>
																		<input type="text" _4_name="name" name="uiJobSizeFlat4" value="" size="40" id="uiJobSizeFlat4"><br>
																		(Example: 17&quot;w x 11&quot;h)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Size Folded:</b><br>
																		<input type="text" name="uiJobSizeFolded4" size="40" id="uiJobSizeFolded4"><br>
																		(Example: 8.5&quot;w x 11&quot;h)</td>
																	<td valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Side(s):</b><br>
																		<input type="text" _4_name="name" name="uiJobSides4" value="" size="40" id="uiJobSides4"><br>
																		 (Example: Single Sided, Double Sided)<br>
																	</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Ink Color(s):</b><br>
																		<input type="text" name="uiJobColors4" size="40" id="uiJobColors4"><br>
																		 (Example: Black, PMS, 4C Process)</td>
																	<td valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Stock:</b><br>
																		<input type="text" _4_name="name" name="uiJobStock4" value="" size="40" id="uiJobStock4"><br>
																		 (Example: 70 LB White Text, 100 LB Natural Cover)</td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Coating:</b><br>
																		<input type="text" name="uiJobCoating4" size="40" id="uiJobCoating4"><br>
																		 (Example: Matte/Gloss, Aqueous, Varnish, UV) Leave blank for uncoated</td>
																	<td valign="top"></td>
																</tr>
																<tr>
																	<td class="form" colspan="4" valign="top"><b><strong>Bindery Specifications</strong><br>
																		</b>
																		 This project will require the following:<br>
																		<br>
																		
																		 Please check all that apply</td>
																</tr>
																<tr>
																	<td class="body-form2" colspan="4" valign="top">
																		<div class="body-form2">
																			<table class="body-form2" width="100%" border="0" cellspacing="0" cellpadding="0">
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications14" id="uiSpecifications14"> </label> Collating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications24" id="uiSpecifications24"> </label> Folding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications34" id="uiSpecifications34"> </label> Drilling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications44" id="uiSpecifications44"> </label> Numbering&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications54" id="uiSpecifications54"> </label> Padding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications64" id="uiSpecifications64"> </label> Perforation&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications74" id="uiSpecifications74"> </label> Paper Banding</td>
																					<td class="title"><input type="checkbox" name="uiSpecifications84" id="uiSpecifications84"> Perfect Binding&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications94" id="uiSpecifications94"> </label> Saddle Stitching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications104" id="uiSpecifications104"> </label> Stapling&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications114" id="uiSpecifications114"> </label> Stringing</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications124" id="uiSpecifications124"> </label> Tabbing&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications134" id="uiSpecifications134"> </label> 3-Hole Punching</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications144" id="uiSpecifications14"> </label> Die Cutting&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications154" id="uiSpecifications154"> </label> Embossing&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications164" id="uiSpecifications164"> </label> Foil Stamping&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications174" id="uiSpecifications174"> </label> Laminating&nbsp;</td>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications184" id="uiSpecifications184"> </label> GBC Binding&nbsp;</td>
																				</tr>
																				<tr>
																					<td class="title"><label><input type="checkbox" name="uiSpecifications194" id="uiSpecifications194"> </label> Wire-O Binding</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																					<td class="title">&nbsp;</td>
																				</tr>
																			</table>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b>Additional Bindery Specifications:<br>
																		</b><label><textarea name="uiJobAddSpecifications4" id="uiJobAddSpecifications4" cols="45" rows="5"></textarea>
                            </label></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"><b>Comments/Instructions:<br>
																		</b><label><textarea name="uiJobComments4" id="uiJobComments4" cols="40" rows="5"></textarea>
                            </label></td>
																	<td valign="top"></td>
																</tr>
																<tr>
																	<td class="form" valign="top"><b><font color="#8c98a4"><br>
																				<span class="subtitle">FILE UPLOAD</span></font></b></td>
																	<td class="form" valign="top"></td>
																	<td class="form" valign="top"></td>
																	<td valign="top"></td>
																</tr>
																<tr>
																	<td class="form" colspan="4" valign="top">Please upload your zipped, stuffed, or compressed file(s) by clicking &quot;choose file&quot; and then selecting your file.<br>
																		 Our system can accept transfers of up to 100MB per form submission. If your files exceed this limit, you may either<br>
																		 upload more files by submitting an additional form, send us your files on CD or DVD, or call for private ftp access.<br>Be sure to use the same &quot;project name&quot; and/or reference number that was used on the original submission form.</td>
																</tr>
																<tr height="39">
																	<td class="form" height="39"><b>Choose file(s) to be uploaded:</b><br>
																		<label><input type="file" name="uiRelatedFile4" id="uiRelatedFile4">
                              </label></td>
																	<td class="form" height="39"></td>
																	<td class="form" height="39">&nbsp;</td>
																</tr>
															</table>
														<td>
													</tr>
													<tr id="uiAddJobInfo">
														<td class="body-form2"><br>
															<b><font color="#8c98a4"><a class="body-form" href="#" " onClick="ShowIT();return false;">ADD JOB<br>
																	</a></font></b><span class="style1">(Use when submitting multiple elements in a project)                                                <br>
																<br>
															</span></td>
													</tr>
													<tr>
														<td class="text">
															<div align="left">
																<input type="hidden" id="uiTotalJobs" value="1" name="uiTotalJobs" /><input type="submit" name="uiSubmit" value="Submit" id="uiSubmit"><input type="reset" value="Clear"></div>
														</td>
													</tr>
												</table>
											</form>
											<?php
                                         	}
											else
											{
										 ?><span class="subtitle">Your query has been submitted, thanks.</span>                                         <?php
                                         	}
										 ?></div>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
				<tr height="30">
					<td valign="middle" width="1024" height="25"><span class="footer01"><a class="footer01" href="online-order.php">
								<table width="1024" border="0" cellspacing="0" cellpadding="0">
									<tr>
										<td><span class="footer05"><a class="footer05" href="estimate-request.php">Request an Estimate</a></span><span class="footer04"><a class="footer04" href="estimate-request.php">&nbsp;&nbsp;</a>&#x2022;&nbsp;&nbsp;</span><span class="footer05"><a class="footer05" href="online-order.php">Place an Order</a></span></td>
										<td align="right"><img src="../hmg-images/logos/American-Flag.jpg" alt="" height="20" width="33" align="absmiddle" border="0"></td>
									</tr>
								</table>
							</a></span></td>
				</tr>
				<tr>
					<td valign="top" width="1024"><img src="../hmg-images/components/Line.jpg" alt="" height="4" width="1024" border="0"></td>
				</tr>
				<tr height="20">
					<td valign="middle" width="1024" height="25">
						<div align="left">
							<table width="1024" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td>
										<div align="left">
											<span class="footer05"> 5 Sicomac Road, Suite 124, <font color="#dee1e1">North Haledon, NJ 07508</font></span></div>
									</td>
									<td>
										<div align="right">
											<span class="footer">&copy; 2007 High Mountain Graphics&nbsp;</span></div>
									</td>
								</tr>
							</table>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<p></p>
	</body>

</html>