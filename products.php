<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
    <title>Our Products</title>
    <?php echo $head; ?>

    <style media="screen">
      figure {
        min-height: 250px !important;
      }

      .section_header.skincolored {
        padding-top: 0;
      }
    </style>
</head>

<body class="collapsing_header">
  <?php echo $header; ?>
    <section id="slider_wrapper" class="slider_wrapper full_page_photo">
      <div id="main_flexslider" class="flexslider">
        <ul class="slides">
          <li class="item" style="background-image: url(images/products/products.jpg)">
            <div class="container">
              <div class="carousel-caption">
                <h1>We combine <strong>past</strong>, <strong>present</strong> <br>and <strong>future</strong> technologies</h1>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </section>
    <div class="main">
      <section class="hgroup centered">
        <div class="container">
          <h4 style="margin: 0px 4%;">Since its founding in 1995, <strong>High Mountain Graphics</strong> has built an impressive reputation for producing material that is <em>market focused, tasteful, and environmentally friendly</em>.</h4>
        </div>
      </section>
      <section class="service_teasers">
        <div class="container">
          <div class="service_teaser">
            <div class="row vertical-align">
              <div class="service_photo col-sm-12 col-md-12 col-lg-5">
                <figure style="background-image:url(images/products/offset-printing.jpg); background-size: auto auto;"></figure>
              </div>
              <div class="service_details col-sm-12 col-md-12 col-lg-7">
                <h2 class="section_header skincolored"><b>OFFSET </b>PRINTING</h2>
                <p style="margin-right: 4%;">Offset printing is by far the more dominant form of commercial printing. We specialize in custom Pantone® PMS Spot color printing on certified recycled paper and tree-free paper along with vegetable based soy inks, and low VOC solvents. Spot color printing uses significantly less paper, ink, solvents, and energy than full-color commercial offset printing.</p>
              </div>
            </div>
          </div>
          <div class="service_teaser right">
            <div class="row vertical-align">
              <div class="service_details col-sm-12 col-md-12 col-lg-7 wow animated slideInLeft">
                <h2 class="section_header skincolored"><b>DIGITAL </b>PRINTING</h2>
                <p style="margin-left: 1%;">When turnaround is a priority, digital printing is a cost-effective solution. Whether it's high volume black &amp; white or short run color with variable data, printing can be output on a variety of paper, card-stock, labels, envelopes and synthetic substrates. Digital printing with non-toxic organic toners reduces paper waste and avoids the solvents often used in offset printing.</p>
              </div>
              <div class="service_photo col-sm-12 col-md-12 col-lg-5 wow animated slideInRight">
                <figure style="background-image:url(images/products/digital-printing.jpg); background-size: auto auto;"></figure>
              </div>
            </div>
          </div>
          <div class="service_teaser">
            <div class="row vertical-align">
              <div class="service_photo col-sm-12 col-md-12 col-lg-5">
                <figure style="background-image:url(images/products/custom-websites.jpg); background-size: auto auto;"></figure>
              </div>
              <div class="service_details col-sm-12 col-md-12 col-lg-7">
                <h2 class="section_header skincolored"><b>WEBSITES </b>SOLUTIONS</h2>
                <p style="margin-right: 6%;">When it comes to building a superb website, there is no substitute for a solid background in HTML5, CSS3, and Javascript. Our team of experienced designers and developers can build a professional website that accomplishes all of your business and personal objectives. We provide secure and affordable website hosting solutions to fit any budget.</p>
              </div>
            </div>
          </div>
          <div class="service_teaser right">
            <div class="row vertical-align">
              <div class="service_details col-sm-12 col-md-12 col-lg-7 wow animated slideInLeft">
                <h2 class="section_header skincolored"><b>ARCHITECTURAL </b>SIGNAGE</h2>
                <p style="margin-left: 5%;">Long before the written word there were signs—a directional arrow etched in stone, tribal symbols carved in wood. We’ve come a long way from the days of primitive images and materials, yet the need to communicate continues. Combine award winning designs, and thousands of installations, and you will find the quality of our signage is not hard to define.</p>
              </div>
              <div class="service_photo col-sm-12 col-md-12 col-lg-5 wow animated slideInRight">
                <figure style="background-image:url(images/products/architectural-signage.jpg); background-size: auto auto;"></figure>
              </div>
            </div>
          </div>
          <div class="service_teaser">
            <div class="row vertical-align">
              <div class="service_photo col-sm-12 col-md-12 col-lg-5">
                <figure style="background-image:url(images/products/promotional-products.jpg); background-size: auto auto;"></figure>
              </div>
              <div class="service_details col-sm-12 col-md-12 col-lg-7">
                <h2 class="section_header skincolored"><b>PROMOTIONAL </b>PRODUCTS</h2>
                <p style="margin-right: 5%;">High Mountain Graphics’ is dedicated to maintaining your brand’s identity. From pens and mugs to silk-screen t-shirts &amp; tote-bags, we offer a wide variety of custom promotional items at competitive prices. Whether your company is small or large, our team of creative experts is here to help develop the best marketing solution on time and on budget. In a rush? <a href="tel:973-427-5820">Call now!</a></p>
              </div>
            </div>
          </div>
          <div class="service_teaser right">
            <div class="row vertical-align">
              <div class="service_details col-sm-12 col-md-12 col-lg-7 wow animated slideInLeft">
                <h2 class="section_header skincolored"><b>DISCOUNTED</b> PAPER &amp; ENVELOPES</h2>
                <p>
                  Looking for small quantities of paper, card-stock and envelopes? Want to turn your unwanted inventory into profit? <a href="https://www.brokencartons.com/" target="_blank">BrokenCartons.com</a> is an e-commerce marketplace that helps registered members BUY and SELL discounted paper and related products using a co-operative and sustainable platform. Create your listing, upload products &amp; start selling. <a href="https://www.brokencartons.com/" target="_blank" style="font-weight: bold;">Join Now!</a> It’s FREE &amp; easy.
                </p>
              </div>
              <div class="service_photo col-sm-12 col-md-12 col-lg-5 wow animated slideInRight">
                <a href="https://www.brokencartons.com/" target="_blank">
                  <!-- <figure style="background-image:url(http://brokencartons.com/assets/Image/brokencartons2.png); background-position: center center; background-size: auto auto;"></figure> -->
                  <figure style="background-image:url(images/products/discounted-paper.jpg); background-position: center center; background-size: auto auto;"></figure>
                </a>
              </div>
            </div>
          </div>
          <!-- <div class="service_teaser inverted">
            <div class="row">
              <div class="service_photo col-sm-12 col-md-12 col-lg-5">
                <figure style="background-image:url(http://brokencartons.com/assets/Image/brokencartons2.png); background-size: auto auto;"></figure>
              </div>
              <div class="service_details col-sm-12 col-md-12 col-lg-7">
                <h2 class="section_header skincolored"><b>Broken Carton:<small>less than a full carton. (Broken Carton — A carton of paper containing a quantity that is less than a full carton.) i.e. <em>I had to purchase a full carton of paper since my vendor does not sell broken cartons</em>.</small></h2>
                <p><a href="www.brokencartons.com" target="_blank">BrokenCartons.com</a> is an commerce marketplace that helps members buy &amp; sell discounted paper related products using a co-operative and sustainable platform.</p>
              </div>
            </div>
          </div> -->
        </div>
      </section>
      <footer>
        <section class="twitter_feed_wrapper">
          <div class="container">
            <div class="row">
              <div class="twitter_feed_icon col-sm-1 col-md-1"><a href="#twitter"><i class="fa fa-twitter"></i></a></div>
              <div class="col-sm-11 col-md-11">
                <blockquote>
                  <p>I had a great experience with High Mountain Graphics…They were great. The job was done quickly and the owner was amazing to deal with. I will come back again. Great quality work.</p>
                  — Doug P.
                  <br />
                  <br />
                  <a href="./testimonials.php" target="_blank">See All Testimonials</a>
                </blockquote>
              </div>
            </div>
          </div>
        </section>
          <?php echo $copyright; ?>
      </footer>
    </div>
  <?php echo $scripts; ?>
  <script>document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
':35729/livereload.js?snipver=2"></' + 'script>')</script>
</body>

</html>
