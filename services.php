<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
    <title>Our Services</title>
    <?php echo $head; ?>

    <style media="screen">
      figure {
        min-height: 250px !important;
        background-repeat: no-repeat;
        background-position: center center;
      }
      .section_header.skincolored {
        padding-top: 0;
      }

      /*Design*/
      div.service_teaser:nth-child(1) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2) {
        text-align: left;
      }

      /*Marketing*/
      div.service_teaser:nth-child(2) > div:nth-child(1) > div:nth-child(1) > h2:nth-child(1) {
        text-align: left;
      }

      /*Mailing*/
      div.service_teaser:nth-child(3) > div:nth-child(1) > div:nth-child(2) > p:nth-child(2) {
        text-align: left;
      }

      /*Imaging*/
      .service_details.col-sm-12.col-md-12.col-lg-7.wow.slideInLeft.animated > p {
        text-align: left;
      }
      /*Photography*/
      .service_details.col-sm-12.col-md-12.col-lg-7 > p {
        text-align: right;
      }

    </style>
</head>

<body class="collapsing_header">
  <?php echo $header; ?>
    <section id="slider_wrapper" class="slider_wrapper full_page_photo">
      <div id="main_flexslider" class="flexslider">
        <ul class="slides">
          <li class="item" style="background-image: url(images/services/services.jpg)">
            <div class="container">
              <div class="carousel-caption">
                <h1>We are <strong>proud</strong> of the <strong>strong roots</strong> we've <strong>built</strong> in our <strong>community</strong></h1>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </section>
    <div class="main">
      <section class="hgroup centered">
        <div class="container">
          <h4 style="margin: 0px 1%;">Through endless hours of <strong>research</strong> we continue to <strong>educate</strong> ourselves, our staff, and our clients to improve on <strong>our goal</strong> of providing the best in <strong>graphic communications</strong>.</h4>
        </div>
      </section>
      <section class="service_teasers">
        <div class="container">
          <div class="service_teaser">
            <div class="row vertical-align">
              <div class="col-sm-12 col-md-12 col-lg-5">
                <figure style="background-image:url(images/services/design-services.jpg)"></figure>
              </div>
              <div class="service_details col-sm-12 col-md-12 col-lg-7">
                <h2 class="section_header skincolored"><b>DESIGN </b>SERVICES</h2>
                <p style="margin-right: 7%;">Our creative philosophy is both an art and a science—language, images, colors, and layout are rooted in our client’s profile and brand identity. We combine science and technology to communicate ideas through text and images. Graphic Design is important in the sales and marketing of products and services, and is a critical component of brochures and logos.</p>
              </div>
            </div>
          </div>
          <div class="service_teaser right">
            <div class="row vertical-align">
              <div class="service_details col-sm-12 col-md-12 col-lg-7 wow animated slideInLeft">
                <h2 class="section_header skincolored" style="text-align: right;"><b>MARKETING </b>SERVICES</h2>
                <p style="text-align: right; margin-left: 4%;">Planting personality into your product, service or brand is just one of the many ways we can give your business an edge. From digital marketing and print production to keyword research and website optimization, we offer innovative ways to advance your position in the marketplace, economically and sustainably. High Mountain Graphics can bring your ideas to higher ground.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-5 wow animated slideInRight">
                <figure style="background-image:url(images/services/marketing-services.jpg)"></figure>
              </div>
            </div>
          </div>
          <div class="service_teaser">
            <div class="row vertical-align">
              <div class="col-sm-12 col-md-12 col-lg-5">
                <figure style="background-image:url(images/services/mailing-services.jpg)"></figure>
              </div>
              <div class="service_details col-sm-12 col-md-12 col-lg-7">
                <h2 class="section_header skincolored"><b>MAILING </b>SERVICES</h2>
                <p style="margin-right: 5%;">Let High Mountain Graphics deliver your message with our in-house mailing department. Our services include addressing, inserting, match-mailing, pre-sorting, wafer-sealing and variable data. Save money on postage and saturate your local market with Every Door Direct Mail®. High Mountain Graphics is a USPS Preferred Partner offering quality printing and mailing.</p>
              </div>
            </div>
          </div>
          <div class="service_teaser right">
            <div class="row vertical-align">
              <div class="service_details col-sm-12 col-md-12 col-lg-7 wow animated slideInLeft">
                <h2 class="section_header skincolored"><b>BINDERY </b>SERVICES</h2>
                <p style="text-align: right; margin: 0px 3%;">Bindery is the staple of any print project– pun intended! High Mountain Graphics offers a complete array of in-house bindery and finishing services to complete you project on budget, and on-time. We can trim, drill, fold, glue, insert, number, pad, perforate, punch, saddle-stitch, score, shrink-wrap, paper-band, wafer-seal, and pack any job, large or small.</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-5 wow animated slideInRight">
                <figure style="background-image:url(images/services/bindery-services.jpg)"></figure>
              </div>
            </div>
          </div>
          <div class="service_teaser right">
            <div class="row vertical-align">
              <div class="col-sm-12 col-md-12 col-lg-5 wow animated slideInLeft">
                <figure style="background-image:url(images/services/milagro.gif); background-size: auto 100%;"></figure>
              </div>
              <div class="service_details col-sm-12 col-md-12 col-lg-7 wow animated slideInRight">
                <h2 class="section_header skincolored" style="text-align: left;"><b>IMAGING </b>SERVICES</h2>
                <p style="margin-right: 3%;">Let one of our talented artists put a new face on your business. Our high-end imaging services include conventional and digital photography, flatbed and drum scanning, photo-retouching and restoration, composition, color-correction. Complete your project with Large Format Printing on a variety of material like aluminum, canvas, card-stock, vinyl and more.</p>
              </div>
            </div>
          </div>
          <div class="service_teaser">
            <div class="row vertical-align">
              <div class="service_details col-sm-12 col-md-12 col-lg-7">
                <h2 class="section_header skincolored" style="text-align: right;"><b>PHOTOGRAPHY </b>SERVICES</h2>
                <p>Whether preserving your favorite memories or capturing special moments, the subject seems to announce it’s presence. They reveal their form through a well seen and carefully composed photograph. Our professional photographers specialize in landscape, portrait, product, and wedding photography. It’s our business to make your business look great!</p>
              </div>
              <div class="col-sm-12 col-md-12 col-lg-5">
                <figure style="background-image:url(images/services/photography-services.jpg); background-size: auto 100%;"></figure>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer>
        <section class="twitter_feed_wrapper">
          <div class="container">
            <div class="row">
              <div class="twitter_feed_icon col-sm-1 col-md-1"><a href="#twitter"><i class="fa fa-twitter"></i></a></div>
              <div class="col-sm-11 col-md-11">
                <blockquote>
                  <p>Rick was fantastic throughout the entire process and very responsive to all my e-mails. Quality of business cards was outstanding. Don't waste your time buying business cards online to save a few dollars. Support your local printer and contact High Mountain Graphics today!</p>
                  — M.P.
                  <br />
                  <br />
                  <a href="./testimonials.php" target="_blank">See All Testimonials</a>
                </blockquote>
              </div>
            </div>
          </div>
        </section>
          <?php echo $copyright; ?>
      </footer>
    </div>
  <?php echo $scripts; ?>
</body>

</html>
