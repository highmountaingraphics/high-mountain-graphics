<!DOCTYPE html>
<html>

<head>
  <title>About Us</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!-- Styles -->
  <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/compiled/bootstrap-overrides.css" type="text/css" />
  <link rel="stylesheet" type="text/css" href="css/compiled/theme.css" />

  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css' />

  <link rel="stylesheet" href="css/compiled/about.css" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/lib/animate.css" media="screen, projection" />
  <link rel="stylesheet" href="css/lib/flexslider.css" type="text/css" media="screen" />

  <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  <?php include 'footer.php'; ?>
</head>

<body>
  <div class="navbar navbar-inverse navbar-static-top" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a href="index.php" class="navbar-brand"><strong>HIGH MOUNTAIN GRAPHICS</strong></a>
      </div>

      <div class="collapse navbar-collapse navbar-ex1-collapse" role="navigation">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="index.php">HOME</a>
          </li>
          <li class="active"><a href="about-us.php">ABOUT US</a>
          </li>
          <li><a href="services.php">SERVICES</a>
          </li>
          <li><a href="portfolio.php">PORTFOLIO</a>
          </li>
          <li><a href="resources.php">RESOURCES</a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">CONTACT <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="contact.php">Contact Us</a>
              </li>
              <li><a href="estimate.php">Request an Estimate</a>
              </li>
              <li><a href="order.php">Place an Order</a>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>

  <div id="aboutus">
    <div class="container">
      <!--             <div class="section_header">
                <h3>About Us</h3>
            </div> -->
      <div class="row">
        <div class="col-sm-6 intro">
          <h6>Graphics & Printing&#133;<br>
                    Consider the nature of your communcations.</h6>
          <p>
            High Mountain Graphics is a Northern New Jersey Firm specializing in Environmentally Friendly Concept to Completion Graphic Communications for businesses and individuals throughout the Northeast and abroad. Growing concerns with the environment have prompted us to do our part and act responsibly as a company. By adopting green practices and guidelines we minimize impact on the environment and retain maximum quality and value for the client.
            <br />
          </p>
        </div>
        <div class="col-sm-6">
          <div class="flexslider">
            <ul class="slides">
              <li>
                <img src="img/about_slide1.jpg" />
              </li>
              <li>
                <img src="img/about_slide1.jpg" />
              </li>
              <li>
                <img src="img/about_slide1.jpg" />
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 intro">
          <p>By choosing FSC Certified, 100% Post Consumer Waste (PCW), Processed Chlorine Free (PCF), recycled paper, along with vegetable based soy ink, manufactured locally, and low VOC solvents, we're truly doing more with less. Using paper, ink and supplies with these and other certifications is a testimony of our commitment to provide intelligent choices for our clients while promoting environmentally friendly practices for sustainability.</p>
        </div>
        <div class="col-sm-6 intro">
          <p>When it comes to experience, we have mountains of it. Our unquenchable and passionate drive allows us to research continuously, seeking out eco-friendly solutions that meet and exceed our client’s marketing and business objectives. We pride ourselves on being the complete source for all of your graphic and printing needs. Choosing to work with High Mountain Graphics means partnering with a reliable company that is environmentally responsible.</p>
        </div>
      </div>
    </div>
  </div>

  <div id="team">
    <div class="container">
      <div class="section_header">
        <h3>We are a family owned & operated business</h3>
        <p>We are proud of the strong roots we've built in our community and are committed to extending this service abroad.</p>
      </div>

      <div class="row people">
        <div class="row row1">
          <div class="col-sm-6 bio_box">
            <img src="img/ale.png" alt="">
            <div class="info">
              <p class="name">Richard Shields</p>
              <p class="area">President</p>
              <p class="area">(973) 809-2282</p>
              <p class="area">rick@highmountaingraphics.com</p>
              <a href="#" class="facebook">
                <span class="socialicons ico1"></span>
                <span class="socialicons_h ico1h"></span>
              </a>
              <a href="#" class="twitter">
                <span class="socialicons ico2"></span>
                <span class="socialicons_h ico2h"></span>
              </a>
              <a href="#" class="flickr">
                <span class="socialicons ico4"></span>
                <span class="socialicons_h ico4h"></span>
              </a>
              <a href="#" class="dribble">
                <span class="socialicons ico6"></span>
                <span class="socialicons_h ico6h"></span>
              </a>
            </div>
          </div>

          <div class="col-sm-6 bio_box bio_boxr">
            <img src="img/ale.png" alt="">
            <div class="info">
              <p class="name">Teresa Ann Shields</p>
              <p class="area">Sales & Administration</p>
              <p class="area">(973) 427-5820</p>
              <p class="area">terri@highmountaingraphics.com</p>
              <a href="#" class="facebook">
                <span class="socialicons ico1"></span>
                <span class="socialicons_h ico1h"></span>
              </a>
              <a href="#" class="twitter">
                <span class="socialicons ico2"></span>
                <span class="socialicons_h ico2h"></span>
              </a>
              <a href="#" class="flickr">
                <span class="socialicons ico4"></span>
                <span class="socialicons_h ico4h"></span>
              </a>
              <a href="#" class="dribble">
                <span class="socialicons ico6"></span>
                <span class="socialicons_h ico6h"></span>
              </a>
            </div>
          </div>
        </div>

        <div class="row row1">
          <div class="col-sm-6 bio_box">
            <img src="img/ale.png" alt="">
            <div class="info">
              <p class="name">RikkyLynn Shields</p>
              <p class="area">Social Media</p>
              <p class="area">(973) 427-5820</p>
              <p class="area">rikkilynn@highmountaingraphics.com</p>
              <a href="#" class="facebook">
                <span class="socialicons ico1"></span>
                <span class="socialicons_h ico1h"></span>
              </a>
              <a href="#" class="twitter">
                <span class="socialicons ico2"></span>
                <span class="socialicons_h ico2h"></span>
              </a>
              <a href="#" class="flickr">
                <span class="socialicons ico4"></span>
                <span class="socialicons_h ico4h"></span>
              </a>
              <a href="#" class="dribble">
                <span class="socialicons ico6"></span>
                <span class="socialicons_h ico6h"></span>
              </a>
            </div>
          </div>

          <div class="col-sm-6 bio_box bio_boxr">
            <img src="img/ale.png" alt="">
            <div class="info">
              <p class="name">Steven G. Sanderson</p>
              <p class="area">Production & Sales</p>
              <p class="area">(973) 229-3185</p>
              <p class="area">steve@highmountaingraphics.com</p>
              <a href="#" class="facebook">
                <span class="socialicons ico1"></span>
                <span class="socialicons_h ico1h"></span>
              </a>
              <a href="#" class="twitter">
                <span class="socialicons ico2"></span>
                <span class="socialicons_h ico2h"></span>
              </a>
              <a href="#" class="flickr">
                <span class="socialicons ico4"></span>
                <span class="socialicons_h ico4h"></span>
              </a>
              <a href="#" class="dribble">
                <span class="socialicons ico6"></span>
                <span class="socialicons_h ico6h"></span>
              </a>
            </div>
          </div>
        </div>
        <div class="row row1">
          <div class="col-sm-6 bio_box bio_boxr">
            <img src="img/ale.png" alt="">
            <div class="info">
              <p class="name">Freddie Baez</p>
              <p class="area">Hispanic Sales</p>
              <p class="area">(973) 477-7463</p>
              <p class="area">freddy@highmountaingraphics.com</p>
              <a href="#" class="facebook">
                <span class="socialicons ico1"></span>
                <span class="socialicons_h ico1h"></span>
              </a>
              <a href="#" class="twitter">
                <span class="socialicons ico2"></span>
                <span class="socialicons_h ico2h"></span>
              </a>
              <a href="#" class="flickr">
                <span class="socialicons ico4"></span>
                <span class="socialicons_h ico4h"></span>
              </a>
              <a href="#" class="dribble">
                <span class="socialicons ico6"></span>
                <span class="socialicons_h ico6h"></span>
              </a>
            </div>
          </div>
          <div class="col-sm-6 bio_box bio_boxr">
            <img src="img/ale.png" alt="">
            <div class="info">
              <p class="name">Anish Kshatriya</p>
              <p class="area">Web Guru</p>
              <p class="area">(973) 427-5820</p>
              <p class="area">anish@highmountaingraphics.com</p>
              <a href="#" class="facebook">
                <span class="socialicons ico1"></span>
                <span class="socialicons_h ico1h"></span>
              </a>
              <a href="#" class="twitter">
                <span class="socialicons ico2"></span>
                <span class="socialicons_h ico2h"></span>
              </a>
              <a href="#" class="flickr">
                <span class="socialicons ico4"></span>
                <span class="socialicons_h ico4h"></span>
              </a>
              <a href="#" class="dribble">
                <span class="socialicons ico6"></span>
                <span class="socialicons_h ico6h"></span>
              </a>
            </div>
          </div>
        </div>
      </div>
        <p>
          <img src="img/adobe.gif">High Mountain Graphics is a registered Adobe Solutions Network Print Service Provider.</p>
      </div>
    </div>
  </div>

  <div id="process">
    <div class="container">
      <!--             <div class="section_header">
                <h3>Our Mission</h3>
            </div> -->
      <div class="row services_circles">
        <div class="col-sm-4 description">
          <div class="text active">
            <h4>Our Mission</h4>
            <p>
              To promote environmentally friendly, socially beneficial, and cost-effective Graphic Communication Services.</p>
          </div>
          <div class="text">
            <h4>Our Promise</h4>
            <p>
              To provide prompt and courteous service, exceptional quality, and competitive pricing to maximize advertising budgets.</p>
          </div>
          <div class="text">
            <h4>Our Goal</h4>
            <p>
              To lead with a passion that inspires others to adopt environmental ethics of preservation and conservation.</p>
          </div>
          <div class="text">
            <h4>Our Commitment</h4>
            <p>
              To be good stewards of the earth and its resources while striving to preserve and protect the world in which we live. One dollar from every online order will go to The Nature Conservancy's mission to preserve the plants, animals, and natural communities that represent the diversity of life on Earth by protecting the lands and waters they need to survive.</p>
            <img src="img/logo-nature.png" alt="The Nature Conservancy">
          </div>
        </div>
        <div class="col-sm-7 areas">
          <div class="row>">
            <div class="col-sm-6">
              <div class="circle active">
                <img src="img/plan.png" />
                <span>Mission</span>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="circle circleOverlapLeft">
                <img src="img/develop.png" />
                <span>Promise</span>
              </div>
            </div>
          </div>
          <div class="row>">
            <div class="col-sm-6">
              <div class="circle circleOverlapTop">
                <img src="img/develop.png" />
                <span>Goal</span>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="circle last_circle circleOverlapLeft circleOverlapTop">
                <img src="img/design.png" />
                <span>Commitment</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <!-- starts footer -->
  <?php echo $footer; ?>

  <script src="http://code.jquery.com/jquery-latest.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/theme.js"></script>
  <script type="text/javascript" src="js/flexslider.js"></script>
</body>

</html>