<?php
$subjectPrefix = '[Contato via Site]';
$emailTo = '
<anish@brokencartons.com>
';

if($_SERVER['REQUEST_METHOD'] == 'POST') {
$name     = stripslashes(trim($_POST['form-name']));
$email    = stripslashes(trim($_POST['form-email']));
$tel      = stripslashes(trim($_POST['form-tel']));
$assunto  = stripslashes(trim($_POST['form-assunto']));
$mensagem = stripslashes(trim($_POST['form-mensagem']));
$pattern  = '/[\r\n]|Content-Type:|Bcc:|Cc:/i';

if (preg_match($pattern, $name) || preg_match($pattern, $email) || preg_match($pattern, $assunto)) {
die("Header injection detected");
}

$emailIsValid = preg_match('/^[^0-9][A-z0-9._%+-]+([.][A-z0-9_]+)*[@][A-z0-9_]+([.][A-z0-9_]+)*[.][A-z]{2,4}$/', $email);

if($name && $email && $emailIsValid && $assunto && $mensagem){
$subject = "$subjectPrefix $assunto";
$body = "Name: $name
<br />
Email: $email
<br />
Phone: $tel
<br />
Message: $mensagem";

$headers  = 'MIME-Version: 1.1' . PHP_EOL;
$headers .= 'Content-type: text/html; charset=utf-8' . PHP_EOL;
$headers .= "From: $name
<$email>
" . PHP_EOL;
$headers .= "Return-Path: $emailTo" . PHP_EOL;
$headers .= "Reply-To: $email" . PHP_EOL;
$headers .= "X-Mailer: PHP/". phpversion() . PHP_EOL;

mail($emailTo, $subject, $body, $headers);
$emailSent = true;

} else {
$hasError = true;
}
}
?>

<!DOCTYPE html>
<html>
  <head>
    <title>
      Contact Us
  </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!-- Styles -->
  <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="css/compiled/bootstrap-overrides.css" type="text/css" />
  <link rel="stylesheet" type="text/css" href="css/compiled/theme.css" />

  <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css' />

  <link rel="stylesheet" href="css/compiled/contact.css" type="text/css" media="screen" />
  <link rel="stylesheet" type="text/css" href="css/lib/animate.css" media="screen, projection" />

  <!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
</script>
<![endif]-->
  <?php include 'footer.php' ?>
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">
              Toggle navigation
            </span>
            <span class="icon-bar">
            </span>
            <span class="icon-bar">
            </span>
            <span class="icon-bar">
            </span>
          </button>
          <a href="index.php" class="navbar-brand">
            <strong>
              HIGH MOUNTAIN GRAPHICS
            </strong>
          </a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse" role="navigation">
          <ul class="nav navbar-nav navbar-right">
            <li>
              <a href="index.php">
                HOME
              </a>
            </li>
            <li>
              <a href="about-us.php">
                ABOUT US
              </a>
            </li>
            <li>
              <a href="services.php">
                SERVICES
              </a>
            </li>
            <li>
              <a href="portfolio.php">
                PORTFOLIO
              </a>
            </li>
            <li>
              <a href="resources.php">
                RESOURCES
              </a>
            </li>
            <li class="dropdown active">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                CONTACT
                <b class="caret">
                </b>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <a href="contact.php">
                    Contact Us
                  </a>
                </li>
                <li>
                  <a href="estimate.php">
                    Request an Estimate
                  </a>
                </li>
                <li>
                  <a href="order.php">
                    Place an Order
                  </a>
                </li>
              </ul>
        </div>
      </div>
  </div>

  <div id="contact">
    <div class="container">
      <div class="section_header">
        <h3>
          Get in touch
        </h3>
      </div>
      <div class="row contact">
        <p>
          Strategically located at the foothills of High Mountain Preserve—one of the largest tracts of forested land in the Piedmont region of Northern New Jersey—lies High Mountain Graphics, an environmentally friendly graphics and printing company ready to serve the tri-state area and abroad.
        </p>
        <?php if(isset($emailSent) && $emailSent): ?>
        <div class="col-md-6 col-md-offset-3">
          <div class="alert alert-success text-center">
            Your message has been sent successfully!
          </div>
              </div>
              <?php else: ?>
              <?php if(isset($hasError) && $hasError): ?>
              <div class="col-md-5 col-md-offset-4">
                <div class="alert alert-danger text-center">
                  Sorry, but there was an error sending the message. Please try again later.
                </div>
              </div>
              <?php endif; ?>
          </div>

          <div class="col-md-6 col-md-offset-3">
            <form action="
<?php echo $_SERVER['REQUEST_URI']; ?>
" id="contact-form" class="form-horizontal" role="form" method="post">
  <div class="form-group">
    <label for="name" class="col-lg-2 control-label">
      Name
    </label>
    <div class="col-lg-10">
      <input type="text" class="form-control" id="form-name" name="form-name" placeholder="Name" required>
    </div>
  </div>
  <div class="form-group">
    <label for="email" class="col-lg-2 control-label">
      Email
    </label>
    <div class="col-lg-10">
      <input type="email" class="form-control" id="form-email" name="form-email" placeholder="Email" required>
    </div>
  </div>
  <div class="form-group">
    <label for="tel" class="col-lg-2 control-label">
      Phone
    </label>
    <div class="col-lg-10">
      <input type="tel" class="form-control" id="form-tel" name="form-tel" placeholder="Phone">
    </div>
  </div>
  <div class="form-group">
    <label for="assunto" class="col-lg-2 control-label">
      Subject
    </label>
    <div class="col-lg-10">
      <input type="text" class="form-control" id="form-assunto" name="form-assunto" placeholder="Subject" required>
    </div>
  </div>
  <div class="form-group">
    <label for="mensagem" class="col-lg-2 control-label">
      Message
    </label>
    <div class="col-lg-10">
      <textarea class="form-control" rows="3" id="form-mensagem" name="form-mensagem" placeholder="Message" required>
      </textarea>
    </div>
  </div>
  <div class="form-group">
    <div class="col-lg-offset-2 col-lg-10">
      <button type="submit" class="btn btn-default">
        Submit
      </button>
    </div>
  </div>
      </form>
          </div>
          <?php endif; ?>

          <?php
$ieVersion = preg_match('/MSIE (.*?);/', $_SERVER['HTTP_USER_AGENT'], $matches) ? floatval($matches[1]) : null;

if($ieVersion
< 9 && $ieVersion != null) {
$jQueryVersion = '1.10.2';
} else {
$jQueryVersion = '2.0.3';
}
?>
      </div>

      <div class="map">
        <div class="container">
          <div class="box_wrapp">
            <div class="box_cont">
              <div class="head">
                <h6>
                  Contact
                </h6>
              </div>
              <ul class="street">
                <li>
                  110-B Fifth Avenue
                </li>
                <li>
                  Hawthorne, NJ USA
                </li>
                <li class="icon icontop">
                  <span class="contacticos ico1">
                  </span>
                  <span class="text">
                    (973) 427-5820
                  </span>
                </li>
                <li class="icon">
                  <span class="contacticos ico2">
                  </span>
                  <a class="text" href="#">
                    info@highmountaingraphics.com
                  </a>
                </li>
              </ul>

              <div class="head headbottom">
                <h6>
                  Work with us
                </h6>
              </div>
              <p>
                We’ve prepared a simple project planner to get to know you and your project better.
              </p>
              <a href="#" class="btn">
                Let's get started
              </a>
            </div>
          </div>
        </div>
        <iframe width="100%" height="600" frameborder="0" scrolling"no" marginheight="0" src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3013.0543449508073!2d-74.1520786!3d40.9583883!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sus!4v1402765736259" width="800" height="600" frameborder="0" style="border:0">
        </iframe>
      </div>
  </div>

  <!-- starts footer -->
  <?php echo $footer; ?>

  <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/
<?php echo $jQueryVersion; ?>
/jquery.min.js">
  </script>
  <script type="text/javascript" src="assets/js/contact-form.js">
  </script>
  <script src="js/bootstrap.min.js">
  </script>
  <script src="js/theme.js">
  </script>
  </body>
</html>
