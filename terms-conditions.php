<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
    <title>Terms &amp; Conditions</title>
    <?php echo $head; ?>

    <style media="screen">
      p {
        margin: 0;
      }

      p ~ h5 {
        margin-bottom: 0;
      }
    </style>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" class="collapsing_header">
  <?php echo $header; ?>
    <div class="main">
      <section>
        <div class="container">
          <h3><strong>TERMS &amp; CONDITIONS </strong></h3>
          <p>To start work we require a complete and accurate Online Order or Purchase Order. We gladly and securely accept Visa, Mastercard, Amex, Electronic Check Payment and PayPal.</p>
          <img src="http://brokencartons.com/assets/Image/pictures/creditcards.png" alt="credit cards">
          <h3><strong>TURNAROUND TIMES:</strong></h3>

          <h5><strong>Standard Turnaround:</strong></h5>
          <p>3-5 Business days after approval for small orders (Shipping not included)</p>
          <p>5-7 Business days after approval for medium orders (Shipping not included)</p>
          <p>7-10 Business days after approval for large projects (Shipping not included)</p>

          <h5><strong>Rush Turnaround:</strong></h5>
          <p>1-2 Business days after approval (Shipping not included)</p>
          <p>There is a 25% to 50% fee for Rush Service.</p>

          <h5><strong>Same Day &amp; Next Day Turnaround:</strong></h5>
          <p>Call for our same day printing &amp; shipping services.</p>

          <h5><strong>Co-operative &amp; Non-profit Turnaround:</strong></h5>
          <p>Ask about our co-op &amp; non-profit programs. We offer a 5% to 10% courtesy discount for adding our tagline: Printing with the environment in mind: www.highmountaingraphics.com.</p>

          <h5><strong>Estimates:</strong></h5>
          <p>Estimates are valid for thirty days. Estimates not accepted within this time are subject to review and may be adjusted. The fees and expenses shown are minimum estimates only. Final fees and expenses shall be shown when invoice is rendered. The Client’s approval shall be obtained for any increases in fees or expenses that exceed the original estimate by ten percent (10%) or more.</p>

          <h5><strong>Orders:</strong></h5>
          <p>Acceptance of orders is subject to credit approval. Canceled orders require compensation for incurred costs and related obligations.</p>

          <h5><strong>Time for Payment:</strong></h5>
          <p>All invoices are due upon completion and payment must be received prior to shipping. A 1 1/2% monthly service charge is payable on all overdue balances. The grant of any license or right of copyright is conditioned on receipt of full payment.</p>

          <h5><strong>Default in Payment:</strong></h5>
          <p>The Client shall assume responsibility for all collection of legal fees necessitated by default in payment.</p>

          <h5><strong>Taxes:</strong></h5>
          <p>All amounts due for taxes and assessments will be added to the Client's invoice and are the responsibility of the Client. No tax exemption will be granted unless the Client’s “Exemption Certificate” (or other official proof of exemption) accompanies the purchase order. If, after the Client has paid the invoice, it is determined that more tax is due, then the Client must promptly remit the required taxes to the taxing authority, or immediately reimburse High Mountain Graphics for any additional taxes paid.</p>

          <h5><strong>Accuracy of Specifications:</strong></h5>
          <p>Quotations are based on the accuracy of the specifications provided. High Mountain Graphics can re-quote a job at time of submission if specifications don’t conform to the information on which the original quotation was based.</p>

          <h5><strong>Quantity:</strong></h5>
          <p>Quantities are subject to an industry standard of plus or minus (+/-) ten percent (10% over/under) and is billed accordingly. High Mountain Graphics will bill for actual quantity delivered within this tolerance. If the Client requires a guaranteed quantity, the percentage of tolerance must be stated at the time of quotation.</p>

          <h5><strong>Author’s Alterations:</strong></h5>
          <p>Client alterations include all work performed in addition to the original specifications. All such work will be billed at High Mountain Graphics current rates.</p>

          <h5><strong>Experimental Work:</strong></h5>
          <p>Experimental or preliminary work performed at the Client’s request are billable at High Mountain Graphics current rates. This work cannot be used without High Mountain Graphics written consent.</p>

          <h5><strong>Creative Work:</strong></h5>
          <p>Sketches, copy, dummies, and all other creative work developed or furnished by High Mountain Graphics are High Mountain Graphics exclusive property. High Mountain Graphics must give written approval for all use of this work and for any derivation of ideas from it.</p>

          <h5><strong>Color Proofing:</strong></h5>
          <p>Because of differences in equipment, paper, inks, and other conditions between color proofing, production, and pressroom operations, a reasonable variation in color between color proofs and the completed job is to be expected. When variation of this kind occurs, it will be considered acceptable performance.</p>

          <h5><strong>Press Proofs:</strong></h5>
          <p>Press proofs will not be furnished unless they have been requested in the Online Order. A press proof submitted for Client’s approval must be signed and returned marked “O.K.,” “O.K. With Corrections,” or “Revised Proof Required”. Any alterations or corrections made because of the Client’s delay or change of mind will be billed at High Mountain Graphics current rates. High Mountain Graphics will not be responsible for undetected production errors if: 1) proofs are not required by the customer; 2) the work is printed per the customer’s O.K.; 3) requests for changes are communicated orally.</p>

          <h5><strong>Cancellation:</strong></h5>
          <p>In the event an order is cancelled, ownership of all copyrights and the original artwork shall be retained by High Mountain Graphics. A cancellation fee for work completed, based on the contract price and expenses already incurred, shall be paid by the Client.</p>

          <h5><strong>Ownership and Return of Artwork:</strong></h5>
          <p>High Mountain Graphics retains ownership of all original artwork, whether preliminary or final, and the Client shall return such artwork within thirty (30) days of use unless indicated.</p>

          <h5><strong>Samples:</strong></h5>
          <p>We occasionally include Client printed samples in our Sample Kits. If you want to exclude your printed samples, please email <a href="mailto:info@highmountaingraphics.com">info@highmountaingraphics.com</a> or call us at (973) 427-5820.</p>

          <h5><strong>Credit Lines:</strong></h5>
          <p>High Mountain Graphics and any other creators shall receive a credit line with any editorial usage. If similar credit lines are to be given with other types of usage, it must be so indicated. Ask about discounts given for granting High Mountain Graphics a tasteful credit line.</p>

          <h5><strong>Releases:</strong></h5>
          <p>The Client shall indemnify High Mountain Graphics against all claims and expenses, including reasonable attorney’s fees, due to uses for which no release was requested in writing or for uses which exceed authority granted by a release.</p>

          <h5><strong>Modifications:</strong></h5>
          <p>Modification of the Agreement must be written, except that the invoice may include, and the Client shall pay, fees or expenses that were orally authorized in order to progress promptly with the work.</p>

          <h5><strong>Production Schedules:</strong></h5>
          <p>Production schedules will be established and followed by both the Client and High Mountain Graphics. In the event that production schedules are not adhered to by the Client, delivery dates will be subject to review. There will be no liability or penalty for delays due to state of war, riot, civil disorder, fire, strikes, accidents, action of government or civil authority, acts of God, or other causes beyond the control of High Mountain Graphics. In such cases, schedules will be extended by an amount of time equal to delay incurred.</p>

          <h5><strong>Delivery:</strong></h5>
          <p>Unless otherwise specified, the price quoted is for a single shipment, without storage, F.O.B. our platform. Proposals are based on continuous and uninterrupted delivery of the complete order. If the specifications state otherwise, High Mountain Graphics will charge accordingly at current rates. Charges for delivery of materials and supplies from the Client to High Mountain Graphics, or from the Client’s supplier to High Mountain Graphics, are not included in quotations unless specified. Title for finished work passes to the Client upon delivery to the carrier at shipping point; or upon mailing of invoices for the finished work or its segments, whichever occurs first.</p>

          <h5><strong>Uniform Commercial Code:</strong></h5>
          <p>The above terms incorporate Article 2 of the Uniform Commercial Code.</p>

          <h5><strong>Code of Fair Practice:</strong></h5>
          <p>The Client and High Mountain Graphics agree to comply with the provisions of the Code of Fair Practice, a copy of which may be obtained from the Joint Ethics Committee, P.O. Box 179, Grand Central Station, New York, New York, 10017.</p>

          <h5><strong>Warranty of Originality:</strong></h5>
          <p>High Mountain Graphics warrants and represents that, to the best of our knowledge, the work assigned hereunder is original and has not been previously published, or that consent to use has been obtained on an unlimited basis; that all work or portions thereof obtained through the undersigned from third parties is original or, if previously published, that consent to use has been obtained on an unlimited basis; that High Mountain Graphics has full authority to make this agreement; and that the work prepared by the High Mountain Graphics does not contain any scandalous, libelous, or unlawful matter. This warranty does not extend to any uses that the Client or others may make of High Mountain Graphics product which may infringe on the rights of others. Client expressly agrees that it will hold the High Mountain Graphics harmless for all liability caused by the Client’s use of the Designer’s product to the extent such use infringes on the rights of others.</p>

          <h5><strong>Limitation of Liability:</strong></h5>
          <p>Client agrees that it shall not hold High Mountain Graphics or it's employees liable for any incidental or consequential damages which arise from High Mountain Graphics failure to perform any aspect of the Project in a timely manner, regardless of whether such failure was caused by intentional or negligent acts or omissions of High Mountain Graphics or a third party.</p>

          <h5><strong>Dispute Resolution:</strong></h5>
          <p>Any disputes in excess of $500.00 arising out of this Agreement shall be submitted to binding arbitration before the Joint Ethics Committee or a mutually agreed upon arbitrator pursuant to the rules of the American Arbitration Association. The Arbitrator’s award shall be final, and judgment may be entered in any court having jurisdiction thereof. The Client shall pay all arbitration and court costs, reasonable attorney’s fees, and legal interest on any award of judgment in favor of High Mountain Graphics.</p>

          <h5><strong>Acceptance of Terms:</strong></h5>
          <p>By using the High Mountain Graphics web site (www.highmountaingraphics.com), you agree to all terms and conditions detailed above. © <script>document.write(new Date().getFullYear())</script> High Mountain Graphics, llc. All rights reserved. Reproduction or distribution of content, in full or in part, including graphic elements, logos, proprietary images, names and descriptions, and/or any other written or visual content from this site for commercial purposes without written permission from High Mountain Graphics is strictly prohibited. Thank You!</p>
        </div>
      </section>
      <footer>
        <?php echo $footer_teasers_wrapper; ?>
          <?php echo $copyright; ?>
      </footer>
    </div>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <script>
      window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')
    </script>
    <?php echo $scripts; ?>
</body>

</html>
