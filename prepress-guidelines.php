<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
  <title>Prepress Guidelines</title>
  <?php echo $head; ?>

  <style media="screen">
    p ~ h5 {
      margin-bottom: 0;
    }
  </style>
</head>

<body data-spy="scroll" data-target=".bs-docs-sidebar" class="collapsing_header">
  <?php echo $header; ?>
  <div class="main">
    <section>
      <div class="container">
        <h3><strong>PREPRESS GUIDELINES &amp; FILE SUBMISSION</strong></h3>
        <p>The following are guidelines for preparing and submitting your files for printing.</p>
        <p>We prefer native files accompanied by a High-Resolution PDF with bleed and trim marks. When supplying native files e.g. Adobe InDesign® or QuarkXpress™, please include all elements; documents, images, and fonts. Place in one folder and zip or compress before attaching or uploading. PDF files are the standard format for transmitting files for printing, but it's important to prepare your PDF file correctly. Improperly prepared files can delay your job and result in additional charges.</p>
        <h5><strong>Supported Software:</strong> Macintosh &amp; PC</p>
        <p>We accept Adobe Acrobat®, Adobe Illustrator®, Adobe InDesign®, Adobe Photoshop®, Adobe Pagemaker®, CorelDraw®, Corel Paint Shop Pro®, Microsoft Word®, Microsoft Publisher®, and QuarkXPress™ files. If your application is not on this list, please call for other possibilities. High Mountain Graphics is a registered Adobe® Solutions Network Print Service Provider.</p>
        <img src="/images/adobe.png" alt="adobe print service provider" />
        <hr>
        <h3><strong>GENERAL TIPS FOR CREATING &amp; SUBMITTING YOUR FILES</strong></h3>
        <h5><strong>Fonts</strong></h5>
        <p>Please include both screen and printer fonts for each typeface used in your document. These should be placed in a sub folder named "Fonts". High Mountain Graphics prefers Postscript fonts on the Macintosh; however, we accept all font formats. Fonts should be converted to outlines when possible. Be sure to save an editable copy for future changes or corrections.</p>

        <h5><strong>Images</strong></h5>
        <p>Be sure to include all images and graphics used, even if embedded. They should be saved as Grayscale, CMYK or Multichannel for offset or digital printing applications. Crop and size your images, scanning them at the appropriate dimension for your document. Raster color or grayscale images should have a minimum of 300dpi at final size. Bitmapped images or line art should have a minimum of 1200 dpi at final size.</p>

        <h5><strong>Specifying Colors</strong></h5>
        <p>We highly recommend using a printed swatch book for specifying your Pantone® inks. For more information please visit the Pantone® website. These will provide accurate representations of how your color will print. Your monitor does not display color correctly and should not be relied on for precise color control. Be sure to use the correct books for coated or uncoated paper. Use the correct Pantone module in a program for printing process simulation of Pantone colors. High Mountain Graphics carries Pantone (PMS) Swatch Books in coated and uncoated stock. Request a PMS Chip with your proof for accurate color proofing.</p>

        <h5><strong>Pantone® Spot, CMYK &amp; RGB Colors</strong></h5>
        <p>Please be sure to define all colors according to desired output. Printing the file as separations is a good way of checking that all the colors have been defined correctly as process. Adobe InDesign® and Adobe Acrobat® Professional offer accurate viewing of color separations in "Output Preview". The RGB color space should only be used for digital printing or online viewing.</p>

        <h5><strong>Trapping</strong></h5>
        <p>Do not trap your files. We will take care of the trapping for you at no charge. Set knock-outs and overprints accurately.</p>

        <h5><strong>Black Limit</strong></h5>
        <p>Black Limit (BL) should be set between 80% and 93%. This is the maximum percentage black will print in the darkest color. On uncoated or matte paper, the recommended black limit is 80% and for coated paper, 93%. This recommendation applies to halftone images not type.</p>

        <h5><strong>Halftone Information</strong></h5>
        <p>Do not use any of the options that allow you to specify the line screen (lpi), angle or dot shape and save this information in the file. Specifying the incorrect options may lead to moiré patterns in your printed piece. We'll set the correct options when preflighting your file.</p>

        <h5><strong>Transfer Function</strong></h5>
        <p>Do not use this function if your program provides it. The transfer function is used for calibration, so that the requested dot percentage is the same dot percentage reproduced on film. Image-setters are calibrated with a transfer function optimized for particular printing presses. Any transfer function embedded in a file will override this calibration and will not produce optimum results.</p>

        <h5><strong>Clipping Paths</strong></h5>
        <p>Beware of clipping paths. Clipping paths are very memory intensive. Images clipped with complex clipping may not print on the imagesetter even if they print on your laser printer. Fewer calculations are required to print a clipping path on a 300 dpi printer than on a 2540 dpi imagesetter. A few ways to avoid complex paths: Do not use clipping paths to place a silhouetted bitmap on background in another program; create the bitmap with the background as part of a single image in Photoshop. Keep the path simple, such as an oval. Increase the flatness if the flatness feature is provided in your program.</p>

        <h5><strong>Nesting Files</strong></h5>
        <p>Do not nest or embed EPS files in EPS files more than two deep. EPS files nested too deeply may not get the required fonts downloaded when printed.</p>

        <h5><strong>Line Weight</strong></h5>
        <p>Line weight or thicknesses should be defined with a minimum of .25 pt or they may not print. Artwork with lines or rules must be scaled and placed at 100% in page layout application.</p>
        <hr>
        <h3><strong>PROOF TYPES</strong></h3>
        <h5><strong>Soft Proof</strong></h5>
        <p>Shortly after receiving your order and files, you will be emailed a PDF proof for your approval. Soft proofing saves time, money, and is environmentally friendly. You can reply with any comments, corrections or changes and are assured your final approved files are being printed. The first PDF proof is free with each order. There is a small fee for submitting revised files.</p>

        <h5><strong>Digital Proof</strong></h5>
        <p>Though many of today's color printers and digital color proofing systems generate excellent detail and bright colors, none of these systems generates consistently accurate colors when compared to process or spot color offset printing. We provide digital laser proofs accompanied with Pantone PMS Chips for low cost color proofing. When color is critical, ask for a press proofs.</p>

        <h5><strong>Press Proof</strong></h5>
        <p>A press proof is printed using the actual files, paper, inks and printing press specified for the job. A press proof is used to verify images, tonal values, colors, content and position. Because it involves setting up the job and printing to actual specifications, it is normally done when color is critical. Our policy is to match spot colors to the appropriate swatch book and cmyk to industry accepted pleasing color.</p>
        <hr>
        <h3><strong>DISC &amp; FILE SUBMISSION</strong></h3>
        <h5><strong>Accepted Media:</strong></h5>
        <p>High Mountain Graphics recommends that all files be zipped or compressed and uploaded with your Online Order or Estimate Request. We also accept files on CD, DVD, or USB Drives. Supplied discs must be accompanied with b&amp;w or color lasers and a printed directory. All disks will be returned with the final printed piece. We also accept hard copy originals.</p>

        <h5><strong>Email</strong></h5>
        <p>Due to the large size of most color files, sending them via email is not recommended. However you may email smaller items as attachments, as well as other information to <a href="mailto:info@highmountaingraphics.com">info@highmountaingraphics.com</a>. Questions regarding our environmentally friendly products, services, and capabilities may also be sent to the same address or give us a call at (973) 427-5820. Thank You!</p>
      </div>
    </section>
    <footer>
      <?php echo $copyright; ?>
    </footer>
  </div>
  <script src="http://code.jquery.com/jquery-latest.min.js"></script>
  <script>
    window.jQuery || document.write('<script src="js/jquery-1.9.0.min.js"><\/script>')
  </script>
  <?php echo $scripts; ?>
</body>

</html>
