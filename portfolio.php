<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
    <title>Portfolio - High Mountain Graphics</title>
    <?php echo $head; ?>
</head>

<body>
  <?php echo $header; ?>
    <div class="main">
      <section class="hgroup">
        <div class="container">
          <h1>Portfolio (under construction)</h1>
        </div>
      </section>
      <section>
        <div class="container">
          <ul class="portfolio_filters">
            <li><a href="#" data-filter="*">SHOW ALL</a></li>
            <li><a href="#" data-filter=".cat_graphic_design">DESIGN</a></li>
            <li><a href="#" data-filter=".cat_layout">LAYOUT</a></li>
            <li><a href="#" data-filter=".cat_hangtags">HANGTAGS &amp; STRINGING</a></li>
            <li><a href="#" data-filter=".cat_marketing">MARKETING</a></li>
            <li><a href="#" data-filter=".cat_printing">PRINTING</a></li>
            <li><a href="#" data-filter=".cat_signage">SIGNAGE</a></li>
            <li><a href="#" data-filter=".cat_websites">WEBSITES</a></li>
          </ul>
        </div>
      </section>
      <section class="portfolio_strict">
        <div class="container">
          <div class="row grid">
            <div class="cat_layout cat_graphic_design cat_marketing col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item">
              <div class="portfolio_item">
                <a href="portfolio/hmg.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/portfolio/hmg/hmg-stationery.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <!-- <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p> -->
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="portfolio_item.php">High Moutain Graphics</a></h3>
                  <p>Project</p>
                </div>
              </div>
            </div>
            <div class="cat_hangtags col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item grid-item">
              <div class="portfolio_item">
                <a href="/portfolio/feed.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/portfolio/feed/FEED-RED-LOVE-HANGTAG.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="/portfolio/feed.php">FEED Projects</a></h3>
                  <p>people</p>
                </div>
              </div>
            </div>
            <div class="cat_signage col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item">
              <div class="portfolio_item">
                <a href="/portfolio/various.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/portfolio/various/hmg-sign.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="/portfolio/various.php">Various Projects</a></h3>
                  <p>artists</p>
                </div>
              </div>
            </div>
            <div class="cat_printing col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item">
              <div class="portfolio_item">
                <a href="/portfolio/gift-cards.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/portfolio/gift-cards/Lottery-Gift-Card-a.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="/portfolio/gift-cards.php">Gift Cards</a></h3>
                  <p>people</p>
                </div>
              </div>
            </div>
            <div class="cat_printing col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item">
              <div class="portfolio_item">
                <a href="/portfolio/erosner.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/portfolio/erosner/EROSNER-TIMES-SQUARE-NYC.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="/portfolio/erosner.php">Greeting Cards &amp; Postcards</a></h3>
                  <p>travel</p>
                </div>
              </div>
            </div>
            <div class="cat_printing col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item">
              <div class="portfolio_item">
                <a href="/portfolio/chi.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/portfolio/chi/the-knot-cover.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="/portfolio/chi.php">Wedding Invitations</a></h3>
                  <p>people</p>
                </div>
              </div>
            </div>
            <div class="cat_printing col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item">
              <div class="portfolio_item">
                <a href="/portfolio/rwma.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/portfolio/rwma/kettlebell-pcft.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="/portfolio/rwma.php">Real World Martial Arts</a></h3>
                  <p>poetic</p>
                </div>
              </div>
            </div>
            <div class="cat_promotional col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item">
              <div class="portfolio_item">
                <a href="/portfolio/bmw.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/portfolio/bmw/bmw_orig.jpg)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="/portfolio/bmw.php">BMW</a></h3>
                  <p>artists</p>
                </div>
              </div>
            </div>
            <div class="cat_websites col-xs-12 col-sm-6 col-md-4 col-lg-4 grid-item">
              <div class="portfolio_item">
                <a href="/portfolio/retouching.php" data-path-hover="M 180,190 0,158 0,0 180,0 z">
                  <figure style="background-image:url(images/services/milagro.gif)">
                    <svg viewBox="0 0 180 320" preserveAspectRatio="none">
                      <path d="M 180,0 0,0 0,0 180,0 z" />
                    </svg>
                    <figcaption>
                      <p>Description of the project dapibus, tellus ac cursus commodo, mauesris condime ntum nibh, ut fermentum....</p>
                      <div class="view_button">View</div>
                    </figcaption>
                  </figure>
                </a>
                <div class="portfolio_description">
                  <h3><a href="/portfolio/retouching.php">Photo Retouching</a></h3>
                  <p>Imaging</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <footer>
          <?php echo $copyright; ?>
      </footer>
    </div>
    <?php echo $scripts; ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.0/isotope.pkgd.min.js" type="text/javascript"></script>
    <script type="text/javascript">
      var $grid = $('.grid').isotope({
        itemSelector: '.grid-item',
        layoutMode: 'fitRows'
      });
      // filter items on button click
      $('.portfolio_filters').on( 'click', 'a', function() {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
      });

      // filter .metal items
      $grid.isotope({ filter: '.cat_graphic_design' });
      $grid.isotope({ filter: '.cat_layout' });
      $grid.isotope({ filter: '.cat_hangtags' });
      $grid.isotope({ filter: '.cat_marketing' });
      $grid.isotope({ filter: '.cat_printing' });
      $grid.isotope({ filter: '.cat_signage' });
      $grid.isotope({ filter: '.cat_websites' });
      // show all items
      $grid.isotope({ filter: '*' });
    </script>
</body>

</php>
