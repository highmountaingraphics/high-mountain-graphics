<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
    <title>Contact Us</title>
    <?php echo $head; ?>

    <style>
      .team_member #social_media_wrapper {
        display: inline-block;
        position: inherit;
        z-index: 101;
        margin-top: -15px;
      }
      .team_member #social_media_wrapper img {
        min-height: 40px;
        min-width: 40px;
      }

      .hgroup h2 {
        margin: 0 19% 0 0;
      }

      .alignable {
        display: inline-block;
        width: 260px;
        margin-bottom: 12px;
      }

      .social-media-widgets {
        display: flex;
        align-items: center;
        justify-content: center;
        margin-top: 9px;
      }

      .social-media-widgets .IN-widget {
        margin-top: 4px;
        padding: 0 15px;
      }

      .social-media-widgets #twitter-widget-0 {
        margin-top: 1px;
      }

      .appointment {
        color: #999;
        -webkit-transition: all 4s ease;
        -moz-transition: all 4s ease;
        -o-transition: all 4s ease;
        -ms-transition: all 4s ease;
        transition: all 4s ease;
        -webkit-animation-name: changeColor;
        -webkit-animation-duration: 5000ms;
        -webkit-animation-iteration-count: infinite;
        animation: changeColor 2s infinite;
        display: inline-block;
        margin-top: 18px;
      }

      /* Chrome, Safari, Opera */
      @-webkit-keyframes changeColor {
          0%   {color: #999;}
          10%	 {color: yellow;}
          20%  {color: purple;}
          30%  {color: green;}
          40%  {color: blue;}
          50%  {color: red;}
          60%  {color: yellow;}
          70%  {color: purple;}
          80%  {color: green;}
          90%  {color: blue;}
          100% {color: white;}
      }

      /* Standard syntax */
      @keyframes changeColor {
          0%   {color: #999;}
          10%	 {color: yellow;}
          20%  {color: purple;}
          30%  {color: LawnGreen;}
          40%  {color: DodgerBlue;}
          50%  {color: red;}
          60%  {color: yellow;}
          70%  {color: purple;}
          80%  {color: LawnGreen;}
          90%  {color: DodgerBlue;}
          100% {color: white;}
      }

      /*Smaller than 641*/
      @media only screen and (max-width: 641px) {
        .social-media-widgets {
          display: none;
        }
      }

      /* facebook widget */
      ._51mx:last-child > ._51m- {
        display: none !important;
      }

      textarea {
        height: 190px !important;
      }
    </style>
</head>

<body class="collapsing_header">
  <?php echo $header; ?>
    <div class="full_page_photo">
      <div id="map"></div>
    </div>
    <div class="main">
      <section class="hgroup">
        <div class="container">
          <h1><strong>CONTACT US</strong></h1>
          <strong>We are a Family Owned &amp; Operated Business</strong>
          <h2>Strategically located at the foothills of High Mountain Preserve—one of the largest tracts of forested land in the Piedmont region of Northern New Jersey—lies High Mountain Graphics, an environmentally conscious graphics, printing and marketing agency.</h2>
          <!-- <img class="yelp-badge" src="./images/contact/yelp.svg" alt="People love us on Yelp!"> -->
        </div>
      </section>
      <section>
        <div class="container">
          <div class="row">
            <div class="office_address col-sm-4 col-md-4">
              <div class="team_member">
                <!-- <img src="images/HMG.jpg" alt="High Mountain Graphics"> -->
                <a class="alignable" href="https://www.alignable.com/north-haledon-nj/high-mountain-graphics-llc"><img title="Highly Recommended by Locals On Alignable" src="https://www.alignable.com/generators/badges/highly_recommended/badge/67248/high-mountain-graphics-llc" style="width: 100%; height: 100%;"></a>
                <br>
                <br>
                <div id="social_media_wrapper">
                  <a target="_blank" href="https://www.facebook.com/highmountaingraphics">
                    <img alt="facebook" src="images/index/social/facebook.png">
                  </a>
                  <a target="_blank" href="https://twitter.com/highmtngraphics">
                    <img alt="twitter" src="images/index/social/twitter.png">
                  </a>
                  <a target="_blank" href="https://plus.google.com/+HighMountainGraphicsHawthorne/about">
                    <img alt="google plus" src="images/index/social/google-plus.png">
                  </a>
                  <a target="_blank" href="https://www.linkedin.com/company/high-mountain-graphics">
                    <img alt="linkedin" src="images/index/social/linkedin.png">
                  </a>
                  <a target="_blank" href="https://www.yelp.com/biz/high-mountain-graphics-north-haledon">
                    <img alt="blog" src="images/index/social/yelp.png">
                  </a>
                </div>

                <address>
                  5 Sicomac Road, Suite 124
                  <br>
                  North Haledon, NJ 07508
                  <br>
                  <a href="mailto:info@highmountaingraphics.com">info@highmountaingraphics.com</a>
                  <br>
                  <a href="tel:1-973-427-5820">(973) 427-5820</a>
                  <br />
                  <strong class="appointment">By Appointment Only!</strong>
                </address>
                <img src="/images/adobe.png" alt="adobe print service provider">
                <br>
                <div class="social-media-widgets">
                  <!-- Facebook Widget -->
                  <div class="fb-like" data-href="https://www.facebook.com/highmountaingraphics/" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false" style="margin-bottom: 6px; margin-top: 8px;"></div>
                  <!-- Google+ Widget -->
                  <!-- <div class="g-follow" data-annotation="none" data-height="20" data-href="https://plus.google.com/101446175079409635761" data-rel="publisher"></div>
                  <br /> -->
                  <!-- LinkedIn Widget -->
                  <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script><script type="IN/FollowCompany" data-id="10043301" data-counter="right"></script>
                  <!-- Twitter Widget -->
                  <!-- <a class="twitter-follow-button" data-show-count="false" href="https://twitter.com/highmtngraphics">Tweet</a> -->
                  <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Printing%20with%20the%20environment%20in%20mind!%20@highmtngraphics%20#environment%20#paper">Tweet</a>
                  <!-- Yelp Widget -->
                  <!-- <div id="yelp-biz-badge-rc-fA_ZF1DhLOh1kTzdsd9FIQ">
                    <a href="http://yelp.com/biz/high-mountain-graphics-north-haledon?utm_medium=badge_reviews&amp;utm_source=biz_review_badge" target="_blank">
                      Check out High Mountain Graphics on Yelp
                    </a>
                  </div> -->
                </div>
              </div>
            </div>
            <div class="contact_form col-sm-8 col-md-8">
              <form name="contact_form" id="contact_form" method="post">
                <div class="row">
                  <div class="col-sm-6 col-md-6">
                    <label>Name</label>
                    <input name="name" id="name" class="form-control" type="text" value="">
                  </div>
                  <div class="col-sm-6 col-md-6">
                    <label>E-mail</label>
                    <input name="email" id="email" class="form-control" type="text" value="">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <label>Subject</label>
                    <input name="subject" id="subject" class="form-control" type="text">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 col-md-12">
                    <label>Message</label>
                    <textarea name="message" id="message" rows="8" class="form-control"></textarea>
                  </div>
                  <div class="col-sm-12 col-md-12">
                    <br/>
                    <a id="submit_btn" class="btn btn-primary" name="submit">Submit Message</a>
                    <span id="notice" class="alert alert-warning alert-dismissable hidden" style="margin-left:20px;">
                    </span>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <footer>
        <?php echo $copyright; ?>
      </footer>
    </div>
    <?php echo $scripts; ?>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxSz6Z4DBZ6CLMB4UK7m2QK0D9KLBzFrg"></script>
      <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
          // The latitude and longitude to center the map (always required)
          // You can find it easily at http://universimmedia.pagesperso-orange.fr/geo/loc.htm
          var myLatlng = new google.maps.LatLng(40.97334, -74.19250);

          // Basic options for a simple Google Map
          // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
          var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: 12,
            // Disable scrollwheel zooming on the map
            scrollwheel: false,
            center: myLatlng,
            // How you would like to style the map.
            // This is where you would paste any style. For example paste a style found on Snazzy Maps.
            styles: [{
              'featureType': 'water',
              'stylers': [{
                'visibility': 'on'
              }, {
                'color': '#428BCA'
              }]
            }, {
              'featureType': 'landscape',
              'stylers': [{
                'color': '#f2e5d4'
              }]
            }, {
              'featureType': 'road.highway',
              'elementType': 'geometry',
              'stylers': [{
                'color': '#c5c6c6'
              }]
            }, {
              'featureType': 'road.arterial',
              'elementType': 'geometry',
              'stylers': [{
                'color': '#e4d7c6'
              }]
            }, {
              'featureType': 'road.local',
              'elementType': 'geometry',
              'stylers': [{
                'color': '#fbfaf7'
              }]
            }, {
              'featureType': 'poi.park',
              'elementType': 'geometry',
              'stylers': [{
                'color': '#c5dac6'
              }]
            }, {
              'featureType': 'administrative',
              'stylers': [{
                'visibility': 'on'
              }, {
                'lightness': 33
              }]
            }, {
              'featureType': 'road'
            }, {
              'featureType': 'poi.park',
              'elementType': 'labels',
              'stylers': [{
                'visibility': 'on'
              }, {
                'lightness': 20
              }]
            }, {}, {
              'featureType': 'road',
              'stylers': [{
                'lightness': 20
              }]
            }]
          };
          // Get the HTML DOM element that will contain your map
          // We are using a div with id="map" seen up in the <body>
          var mapElement = document.getElementById('map');

          // Create the Google Map using out element and options defined above
          var map = new google.maps.Map(mapElement, mapOptions);

          // Put a marker at the center of the map
          var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            title: 'We are right here!'
          });
        }
      </script>
      <!--Contact Form Required Js -->
      <script type="text/javascript" src="js/contact_form.js"></script>

      <!-- Facebook Widget -->
      <div id="fb-root"></div>
      <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Google Plus Widget -->
      <!-- <script type="text/javascript">
        (function() {
          var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
          po.src = 'https://apis.google.com/js/platform.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
      </script> -->

      <!-- Twitter Widget -->
      <script>window.twttr = (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
          t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function(f) {
          t._e.push(f);
        };

        return t;
      }(document, "script", "twitter-wjs"));</script>

      <!-- Yelp Widget -->
      <!-- <script>
        (function(d, t) {var g = d.createElement(t);var s = d.getElementsByTagName(t)[0];g.id = "yelp-biz-badge-script-rc-fA_ZF1DhLOh1kTzdsd9FIQ";g.src = "//yelp.com/biz_badge_js/en_US/rc/fA_ZF1DhLOh1kTzdsd9FIQ.js";s.parentNode.insertBefore(g, s);}(document, 'script'));
      </script> -->
</body>

</html>
