<?php
include_once("../analyticstracking.php");

$head = '
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,300italic" rel="stylesheet" type="text/css">

<meta charset="utf-8">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="../animate.css" type="text/css">
<link rel="stylesheet" href="../js/woothemes-FlexSlider-06b12f8/flexslider.css" type="text/css" media="screen">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prettyPhoto/3.1.6/css/prettyPhoto.min.css" type="text/css" media="screen">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" media="screen">

<link rel="stylesheet/less" href="../less/style.less" type="text/css">
<link rel="stylesheet" href="../css/custom.css" type="text/css">
<script src="../js/less.js"></script>

<!-- Fav and touch icons -->
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../apple-touch-icon-114x114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../apple-touch-icon-72x72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../apple-touch-fa-57x57-precomposed.png">
<link rel="shortcut icon" href="../ favicon.png">
';

$header = '
<header>
  <div class="container">
    <div class="navbar navbar-default" role="navigation">
      <div class="navbar-header">
        <a href="../">
          <img src="../images/HMG.jpg" alt="High Mountain Graphics">
        </a>
        <a class="btn btn-navbar btn-default navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="nb_left pull-left">
							<span class="fa fa-reorder"></span>
          </span>
          <span class="nb_right pull-right">menu</span>
        </a>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="nav pull-right navbar-nav">
          <li><a href="../">Home</a></li>
          <li><a href="../about-us.php">About Us</a></li>
          <li><a href="../products.php">Products</a></li>
          <li><a href="../services.php">Services</a></li>
          <li><a href="../portfolio.php">Portfolio</a></li>
          <li class="dropdown">
          	<a href="../#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          		Resources
          		<span class="caret"></span>
        		</a>
            <ul class="dropdown-menu">
              <li><a href="../prepress-guidelines.php">Prepress Guidelines</a></li>
              <li><a href="../terms-conditions.php">Terms & Conditions</a></li>
              <li><a href="../environmental-policies.php">Environmental Policies</a></li>
              <li><a href="../environmental-associations.php">Environmental Associations</a></li>
              <li><a href="../paper-options.php">Paper Options</a></li>
            </ul>
          </li>
          <li><a href="../contact.php">Contact</a></li>
        </ul>
      </div>
    </div>
    <div id="sign">
      <a href="../forms/estimate/form.php"><span>Request an Estimate!</span></a>
      <a href="../forms/order/form.php"><span>Place an Order!</span></a>
    </div>
    <div id="social_media_wrapper">
      <a href="https://www.facebook.com/highmountaingraphics" target="_blank">
        <img src="../images/index/social/facebook.png" alt="facebook"/>
      </a>
      <a href="https://twitter.com/highmtngraphics" target="_blank">
        <img src="../images/index/social/twitter.png" alt="twitter"/>
      </a>
      <a href="https://plus.google.com/+HighMountainGraphicsHawthorne/about" target="_blank">
        <img src="../images/index/social/google-plus.png" alt="google plus"/>
      </a>
      <a href="https://www.linkedin.com/company/high-mountain-graphics" target="_blank">
        <img src="../images/index/social/linkedin.png" alt="linkedin"/>
      </a>
      <a href="http://brokencartons.com/blog" target="_blank">
        <img src="../images/index/social/blogger.png" alt="blog"/>
      </a>
      <a href="https://www.youtube.com/user/highmountaingraphics" target="_blank">
        <img src="../images/index/social/youtube.png" alt="youtube"/>
      </a>
      <a href="http://www.yelp.com/biz/high-mountain-graphics-north-haledon" target="_blank">
        <img src="../images/index/social/yelp.png" alt="yelp"/>
      </a>
    </div>
  </div>
</header>
';

$copyright = '
<section class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-6">© 1995 - <script>document.write(new Date().getFullYear())</script> High Mountain Graphics, llc. All rights reserved.</div>
			<div class="text-right col-sm-6 col-md-6"> 5 Sicomac Road, Suite 124 · North Haledon · N.J. 07508 (973) 427-5820</div>
		</div>
	</div>
</section>
';

$scripts = '
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="../js/modernizr.custom.48287.js"></script>
<script src="../js/woothemes-FlexSlider-06b12f8/jquery.flexslider-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prettyPhoto/3.1.6/js/jquery.prettyPhoto.min.js" charset="utf-8"></script>
<script src="../js/jquery.ui.totop.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/snap.svg/0.4.1/snap.svg-min.js"></script>
<script src="../js/main.js"></script>
<script src="../js/custom.js"></script>
<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/598352.js"></script>
<!-- End of HubSpot Embed Code -->
';
?>
