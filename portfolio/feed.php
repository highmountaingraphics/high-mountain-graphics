<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
  <title>Feed Projects - High Mountain Graphics</title>
  <?php echo $head; ?>

  <style media="screen">
    .hgroup .container {
      border-bottom: 0;
    }
  </style>
</head>

<body>
  <?php echo $header; ?>
  <div class="main">
    <section class="hgroup">
      <div class="container">
        <h1>FEED Projects</h1>
        <h2>Check out some of our marketing and promotional products.</h2>
        <!-- <ul class="breadcrumb pull-right">
          <li><a href="../index.html">Home</a> </li>
          <li class="active">Portfolio</li>
        </ul> -->
      </div>
    </section>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-md-8">
            <section class="portfolio_slider_wrapper">
              <div class="flexslider" id="portfolio_slider">
                <ul class="slides">
                  <li class="item" data-thumb="../images/portfolio/feed/FEED-RED-LOVE-HANGTAG.jpg" style="background-image: url(../images/portfolio/feed/FEED-RED-LOVE-HANGTAG.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/feed/FEED-RED-LOVE-HANGTAG.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/feed/FEED-3-HANGTAG.jpg" style="background-image: url(../images/portfolio/feed/FEED-3-HANGTAG.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/feed/FEED-3-HANGTAG.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/feed/FEED-25-HANGTAG.jpg" style="background-image: url(../images/portfolio/feed/FEED-25-HANGTAG.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/feed/FEED-25-HANGTAG.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/feed/FEED-BLACK-BAG-HANGTAG.jpg" style="background-image: url(../images/portfolio/feed/FEED-BLACK-BAG-HANGTAG.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/feed/FEED-BLACK-BAG-HANGTAG.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/feed/FEED-VARIOUS-HANGTAGS.jpg" style="background-image: url(../images/portfolio/feed/FEED-VARIOUS-HANGTAGS.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/feed/FEED-VARIOUS-HANGTAGS.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/feed/FEED-COLLAGE.jpg" style="background-image: url(../images/portfolio/feed/FEED-COLLAGE.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/feed/FEED-COLLAGE.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/feed/lauren-bush-lauren-rick-shields.jpg" style="background-image: url(../images/portfolio/feed/lauren-bush-lauren-rick-shields.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/feed/lauren-bush-lauren-rick-shields.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/feed/greg-barber-lauren-bush-lauren-rick-shields.jpg" style="background-image: url(../images/portfolio/feed/greg-barber-lauren-bush-lauren-rick-shields.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/feed/greg-barber-lauren-bush-lauren-rick-shields.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                </ul>
              </div>
              <div id="carousel" class="flexslider">
                <ul class="slides">
                  <li> <img src="../images/portfolio/feed/FEED-RED-LOVE-HANGTAG.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/feed/FEED-3-HANGTAG.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/feed/FEED-25-HANGTAG.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/feed/FEED-BLACK-BAG-HANGTAG.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/feed/FEED-VARIOUS-HANGTAGS.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/feed/FEED-COLLAGE.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/feed/lauren-bush-lauren-rick-shields.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/feed/greg-barber-lauren-bush-lauren-rick-shields.jpg" alt=""> </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-4 col-md-4">
            <article class="portfolio_details">
              <h2 class="section_header">A few words about the project</h2>
              <p>In hac habitasse platea dictumst. In hac habitasse platea dictumst. Donec aliquet tellus enim, a tincidunt nulla. Praesent mollis felis at nulla fermentum mattis. Vivamus vestibulum neque quis nunc convallis venenatis. Nulla tristique lorem sit amet ipsum ornare sit amet feugiat nulla condimentum. Sed faucibus volutpat nunc, at ullamcorper augue elementum id. Quisque at lectus leo, nec placerat mi. Curabitur egestas eleifend interdum. Suspendisse potenti. Suspendisse nec risus fermentum sapien congue fermentum sed at lorem. </p>
              <br>
              <br>
              <div>
                <p><strong>Date:</strong> 2008 - Current</p>
                <p><strong>Client:</strong> FEED Projects</p>
                <p><strong>Location:</strong> New York City</p>
              </div>
              <br>
              <br>
              <!-- <a href="../#" class="btn btn-danger center-block btn-lg">Visit Project Site</a> </article> -->
          </div>
        </div>
      </div>
    </section>
    <!-- <section>
      <div class="container">
        <ul class="pager">
          <li class="previous"><a href="../portfolio_item.html">← Older</a></li>
          <li class="next disabled"><a href="../#">Newer →</a></li>
        </ul>
      </div>
    </section> -->
    <footer>
      <?php echo $copyright; ?>
    </footer>
  </div>
  <?php echo $scripts; ?>
</body>

</html>
