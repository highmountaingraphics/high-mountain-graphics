<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
  <title>Portfolio Item</title>
  <?php echo $head; ?>

  <style media="screen">
    .hgroup .container {
      border-bottom: 0;
    }
  </style>
</head>

<body>
  <?php echo $header; ?>
  <div class="main">
    <section class="hgroup">
      <div class="container">
        <h1>High Mountain Graphics</h1>
        <h2>Check out some of our marketing and promotional products.</h2>
        <!-- <ul class="breadcrumb pull-right">
          <li><a href="../index.html">Home</a> </li>
          <li class="active">Portfolio</li>
        </ul> -->
      </div>
    </section>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-md-8">
            <section class="portfolio_slider_wrapper">
              <div class="flexslider" id="portfolio_slider">
                <ul class="slides">
                  <li class="item" data-thumb="../images/portfolio/hmg/hmg-stationery.jpg" style="background-image: url(../images/portfolio/hmg/hmg-stationery.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/hmg-stationery.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/high-mountain-graphics-business-card-rls.jpg" style="background-image: url(../images/portfolio/hmg/high-mountain-graphics-business-card-rls.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/high-mountain-graphics-business-card-rls.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/hmg-brochure-in.jpg" style="background-image: url(../images/portfolio/hmg/hmg-brochure-in.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/hmg-brochure-in.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/hmg-brochure-out.jpg" style="background-image: url(../images/portfolio/hmg/hmg-brochure-out.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/hmg-brochure-out.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/hmg-sign.jpg" style="background-image: url(../images/portfolio/hmg/hmg-sign.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/hmg-sign.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/hmg-mugs.jpg" style="background-image: url(../images/portfolio/hmg/hmg-mugs.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/hmg-mugs.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/hmg-black-sweatshirt.jpg" style="background-image: url(../images/portfolio/hmg/hmg-black-sweatshirt.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/hmg-black-sweatshirt.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/high-mountain-graphics-white-t-shirt.jpg" style="background-image: url(../images/portfolio/hmg/high-mountain-graphics-white-t-shirt.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/high-mountain-graphics-white-t-shirt.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/hmg-poster-frame.jpg" style="background-image: url(../images/portfolio/hmg/hmg-poster-frame.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/hmg-poster-frame.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/hmg/HMG-Street-Billboard.jpg" style="background-image: url(../images/portfolio/hmg/HMG-Street-Billboard.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/hmg/HMG-Street-Billboard.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                </ul>
              </div>
              <div id="carousel" class="flexslider">
                <ul class="slides">
                  <li> <img src="../images/portfolio/hmg/hmg-stationery.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/high-mountain-graphics-business-card-rls.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/hmg-brochure-in.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/hmg-brochure-out.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/hmg-sign.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/hmg-mugs.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/hmg-black-sweatshirt.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/high-mountain-graphics-white-t-shirt.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/hmg-poster-frame.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/hmg/HMG-Street-Billboard.jpg" alt=""> </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-4 col-md-4">
            <article class="portfolio_details">
              <h2 class="section_header">A few words about the project</h2>
              <p>In hac habitasse platea dictumst. In hac habitasse platea dictumst. Donec aliquet tellus enim, a tincidunt nulla. Praesent mollis felis at nulla fermentum mattis. Vivamus vestibulum neque quis nunc convallis venenatis. Nulla tristique lorem sit amet ipsum ornare sit amet feugiat nulla condimentum. Sed faucibus volutpat nunc, at ullamcorper augue elementum id. Quisque at lectus leo, nec placerat mi. Curabitur egestas eleifend interdum. Suspendisse potenti. Suspendisse nec risus fermentum sapien congue fermentum sed at lorem. </p>
              <br>
              <br>
              <div>
                <p><strong>Date:</strong> 1995 - Current</p>
                <p><strong>Client:</strong> High Moutain Graphics</p>
                <p><strong>Location:</strong> Northern New Jersey</p>
              </div>
              <br>
              <br>
              <!-- <a href="../#" class="btn btn-danger center-block btn-lg">Visit Project Site</a> </article> -->
          </div>
        </div>
      </div>
    </section>
    <!-- <section>
      <div class="container">
        <ul class="pager">
          <li class="previous"><a href="../portfolio_item.html">← Older</a></li>
          <li class="next disabled"><a href="../#">Newer →</a></li>
        </ul>
      </div>
    </section> -->
    <footer>
      <?php echo $copyright; ?>
    </footer>
  </div>
  <?php echo $scripts; ?>
</body>

</html>
