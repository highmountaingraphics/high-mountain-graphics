<!doctype html>
<html class="no-js" lang="en">

<head>
  <?php include "templates.php"; ?>
  <title>Various Projects - High Mountain Graphics</title>
  <?php echo $head; ?>

  <style media="screen">
    .hgroup .container {
      border-bottom: 0;
    }
  </style>
</head>

<body>
  <?php echo $header; ?>
  <div class="main">
    <section class="hgroup">
      <div class="container">
        <h1>Various Projects</h1>
        <h2>Check out some of our marketing and promotional products.</h2>
        <!-- <ul class="breadcrumb pull-right">
          <li><a href="../index.html">Home</a> </li>
          <li class="active">Portfolio</li>
        </ul> -->
      </div>
    </section>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-md-8">
            <section class="portfolio_slider_wrapper">
              <div class="flexslider" id="portfolio_slider">
                <ul class="slides">
                  <li class="item" data-thumb="../images/portfolio/various/hmg-sign.jpg" style="background-image: url(../images/portfolio/various/hmg-sign.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/various/hmg-sign.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/various/hmg-sign-storefront.jpg" style="background-image: url(../images/portfolio/various/hmg-sign-storefront.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/various/hmg-sign-storefront.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/various/hmg-street-billboard.jpg" style="background-image: url(../images/portfolio/various/hmg-street-billboard.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/various/hmg-street-billboard.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/various/Maple-Eyecare-Indoor-Sign.jpg" style="background-image: url(../images/portfolio/various/Maple-Eyecare-Indoor-Sign.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/various/Maple-Eyecare-Indoor-Sign.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/various/jmkm-sign.jpg" style="background-image: url(../images/portfolio/various/jmkm-sign.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/various/jmkm-sign.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                  <li class="item" data-thumb="images/portfolio/various/PETROSSIAN-GOLD-VINYL-GLASS-DOORS.jpg" style="background-image: url(../images/portfolio/various/PETROSSIAN-GOLD-VINYL-GLASS-DOORS.jpg)">
                    <div class="container">
                      <a href="../images/portfolio/various/PETROSSIAN-GOLD-VINYL-GLASS-DOORS.jpg" rel="prettyPhoto[gal]"></a>
                    </div>
                  </li>
                </ul>
              </div>
              <div id="carousel" class="flexslider">
                <ul class="slides">
                  <li> <img src="../images/portfolio/various/hmg-sign.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/various/hmg-sign-storefront.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/various/hmg-street-billboard.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/various/Maple-Eyecare-Indoor-Sign.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/various/jmkm-sign.jpg" alt=""> </li>
                  <li> <img src="../images/portfolio/various/PETROSSIAN-GOLD-VINYL-GLASS-DOORS.jpg" alt=""> </li>
                </ul>
              </div>
            </section>
          </div>
          <div class="col-sm-4 col-md-4">
            <article class="portfolio_details">
              <h2 class="section_header">A few words about the project</h2>
              <p>In hac habitasse platea dictumst. In hac habitasse platea dictumst. Donec aliquet tellus enim, a tincidunt nulla. Praesent mollis felis at nulla fermentum mattis. Vivamus vestibulum neque quis nunc convallis venenatis. Nulla tristique lorem sit amet ipsum ornare sit amet feugiat nulla condimentum. Sed faucibus volutpat nunc, at ullamcorper augue elementum id. Quisque at lectus leo, nec placerat mi. Curabitur egestas eleifend interdum. Suspendisse potenti. Suspendisse nec risus fermentum sapien congue fermentum sed at lorem. </p>
              <br>
              <br>
              <div>
                <p><strong>Date:</strong> 2009 - Current</p>
                <p><strong>Client:</strong> Various Clients</p>
                <p><strong>Location:</strong> NJ / NY</p>
              </div>
              <br>
              <br>
              <!-- <a href="../#" class="btn btn-danger center-block btn-lg">Visit Project Site</a> </article> -->
          </div>
        </div>
      </div>
    </section>
    <!-- <section>
      <div class="container">
        <ul class="pager">
          <li class="previous"><a href="../portfolio_item.html">← Older</a></li>
          <li class="next disabled"><a href="../#">Newer →</a></li>
        </ul>
      </div>
    </section> -->
    <footer>
      <?php echo $copyright; ?>
    </footer>
  </div>
  <?php echo $scripts; ?>
</body>

</html>
