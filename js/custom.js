if (window.location.href.indexOf("/index") > -1) {
  $(".navbar-collapse .nav > li:nth-child(1)").addClass("active");
}

if (window.location.href.indexOf("/about_us") > -1) {
  $(".navbar-collapse .nav > li:nth-child(2)").addClass("active");
}

if (window.location.href.indexOf("/products") > -1) {
  $(".navbar-collapse .nav > li:nth-child(3)").addClass("active");
}

if (window.location.href.indexOf("/services") > -1) {
  $(".navbar-collapse .nav > li:nth-child(4)").addClass("active");
}

if (window.location.href.indexOf("/portfolio") > -1) {
  $(".navbar-collapse .nav > li:nth-child(5)").addClass("active");
}

if (window.location.href.indexOf("/prepress_guidelines") > -1) {
  $(".dropdown-menu > li:nth-child(1)").addClass("active");
}

if (window.location.href.indexOf("/terms_conditions") > -1) {
  $(".dropdown-menu > li:nth-child(2)").addClass("active");
}

if (window.location.href.indexOf("/environmental_policies") > -1) {
  $(".dropdown-menu > li:nth-child(3)").addClass("active");
}

if (window.location.href.indexOf("/environmental_associations") > -1) {
  $(".dropdown-menu > li:nth-child(4)").addClass("active");
}

if (window.location.href.indexOf("/paper_options") > -1) {
  $(".dropdown-menu > li:nth-child(5)").addClass("active");
}

if (window.location.href.indexOf("/contact") > -1) {
  $(".navbar-collapse .nav > li:nth-child(7)").addClass("active");
}

// ===== Scroll to Top ====
$(function () {
  $.scrollUp({
    scrollName: 'scrollUp', // Element ID
    topDistance: '300', // Distance from top before showing element (px)
    topSpeed: 300, // Speed back to top (ms)
    animation: 'fade', // Fade, slide, none
    animationInSpeed: 200, // Animation in speed (ms)
    animationOutSpeed: 200, // Animation out speed (ms)
    scrollText: '', // Text for element
    activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
  });
});
